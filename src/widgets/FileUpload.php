<?php
/**
 * @author    Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 Finally a fast
 * @license   https://www.finally-a-fast.com/packages/fafcms-module-filemanager/license MIT
 * @link      https://www.finally-a-fast.com/packages/fafcms-module-filemanager
 * @see       https://www.finally-a-fast.com/packages/fafcms-module-filemanager/docs Documentation of fafcms-module-filemanager
 * @since     File available since Release 1.0.0
 */

namespace fafcms\filemanager\widgets;

use fafcms\placeholder\Placeholder;
use Yii;
use yii\widgets\InputWidget;
use yii\helpers\Html;

class FileUpload extends InputWidget
{
    public $multiple = true;
    public $fallbackButtonLabel = null;
    public $fallbackButtonLabelOptions = [];
    public $maxFiles;
    public $maxFileSize;
    public $acceptedFileExtensions;
    public $formId;
    private $uploadId;
    private $inputName;

    /**
     * @return string
     */
    public function run(): string
    {
        $this->uploadId = $this->id;
        $this->field->template = '{input}';

        if ($this->hasModel()) {
            $this->uploadId = $options['id'] ?? Html::getInputId($this->model, $this->attribute);
            $this->inputName = $options['name'] ?? Html::getInputName($this->model, $this->attribute);
        } else {
            $this->uploadId = $options['id'] ?? $this->uploadId;
            $this->inputName = $options['name'] ?? $this->name;
        }

        $options = [];

        $options['data'] = [
            'form-id' => $this->formId,
            'input-name' => $this->inputName,
            'max-file-size' => $this->maxFileSize / 1024 / 1024,
            'upload-container' => '.' . $this->uploadId . '-upload-container',
            'max-files' => $this->maxFiles,
            'accepted-file-extensions' => (count($this->acceptedFileExtensions) === 0?'':'.').implode(',.', $this->acceptedFileExtensions)
        ];

        Html::addCssClass($options, 'fafcms-file-upload');

        return Html::tag(
            'div',
                Html::tag(
                'div',
                '<div class="column">
                            <div class="ui segment filemanager-list-item">
                                <div class="ui grid compact">
                                    <div class="row">
                                        <div class="column two wide mobile two wide tablet two wide computer two wide large screen two wide widescreen">
                                            <div class="filemanager-list-item-view">
                                                <div class="filemanager-list-item-icon"><i class="mdi mdi-file"></i></div>
                                            </div>
                                        </div>
                                        <div class="column twelve wide mobile twelve wide tablet twelve wide computer twelve wide large screen twelve wide widescreen">
                                            <div class="text-scroller"><span class="ui small text"><strong data-dz-name class="filemanager-list-item-name"></strong></span></div>
                                            <span class="ui small text"><i class="mdi mdi-harddisk"></i> <span data-dz-size></span></span>
                                            <br>
                                            <div class="ui negative message filemanager-list-item-error hide"></div>
                                            <div class="ui active progress hide" data-percent="42">
                                              <div class="bar" data-dz-uploadprogress>
                                                <div class="progress"></div>
                                              </div>
                                            </div>
                                        </div>
                                        <div class="column two wide mobile two wide tablet two wide computer two wide large screen two wide widescreen">
                                            <div class="ui icon buttons mini">
                                                <button class="ui button filemanager-list-item-delete" data-dz-remove><i class="icon mdi mdi-close-circle-outline"></i></button>
                                                <button class="ui button filemanager-list-item-upload"><i class="mdi mdi-upload-outline"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>',
                ['class' => $this->uploadId . '-preview filemanager-upload-previews-template hide']
            ) . '
                <div class="'.$this->uploadId . '-upload-container filemanager-upload-container">
                    <div class="ui grid compact stretched">
                        <div class="filemanager-upload-previews one column row"></div>
                    </div>
                    <div class="ui placeholder segment dz-message">
                      <div class="ui icon header">
                        <i class="icon mdi mdi-file-plus-outline"></i>
                        ' . Yii::t('fafcms-filemanager', 'Drop files here or click to upload.') . '
                      </div>
                      <div class="ui primary button">' . Yii::t('fafcms-filemanager', 'Add file') . '</div>
                    </div>
                </div>
                <div class="ui message">
                    <p>' . Yii::t('fafcms-filemanager', 'A maximum of {maxFiles} {maxFilesCount,plural,=0{files} =1{file} other{files}} with {maxFileSize} each can be uploaded.', [
                        'maxFiles' => $this->maxFiles,
                        'maxFilesCount' => $this->maxFiles,
                        'maxFileSize' => Yii::$app->formatter->asShortSize($this->maxFileSize, 0),
                    ]) . '</p>
                    <p>' .  Yii::t('fafcms-filemanager', 'Only files with the {extensionCount,plural,=1{extension} other{extensions}} {extensions} are allowed.', [
                        'extensionCount' => count($this->acceptedFileExtensions),
                        'extensions' => implode(', ', $this->acceptedFileExtensions)
                    ]) . '</p>
                </div>
            ',
            $options
        );
    }
}
