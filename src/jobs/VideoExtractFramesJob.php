<?php

namespace fafcms\filemanager\jobs;

use fafcms\filemanager\models\File;
use fafcms\filemanager\models\Filetype;
use fafcms\filemanager\Module;
use FFMpeg\FFMpeg;
use FFMpeg\FFProbe;
use fafcms\filemanager\helpers\ExtractMultipleFramesFilter;
use FFMpeg\Format\Video\X264;
use yii\helpers\StringHelper;

/**
 * Class VideoExtractFramesJob
 * @package fafcms\filemanager\jobs
 */
class VideoExtractFramesJob extends \yii\base\BaseObject implements \yii\queue\RetryableJobInterface
{
    public $fileId;
    public $framePath;
    public $rawFileVttFormatName;
    public $rawFileVttImageFormatName;

    /**
     * @inheritdoc
     */
    public function execute($queue)
    {
        try {
            $file = File::find()->select([
                    File::tableName().'.id',
                    File::tableName().'.allow_download',
                    File::tableName().'.is_public',
                    File::tableName().'.filegroup_id',
                    File::tableName().'.filetype_id',
                    File::tableName().'.filename',
                    Filetype::tableName().'.mediatype',
                    Filetype::tableName().'.mime_type',
                    Filetype::tableName().'.default_extension'
                ])
                ->innerJoinWith('filetype', false)
                ->where([
                    File::tableName().'.id' => $this->fileId
                ])
                ->asArray()
                ->one();

            $fileName = File::getFilePath($file);
            $sourceFormat = $file['default_extension'];

            $ffprobe = FFProbe::create(Module::getLoadedModule()->getFfmpegConfig());
            $duration = $ffprobe->format($fileName)->get('duration');

            $rate = ExtractMultipleFramesFilter::FRAMERATE_EVERY_60SEC;
            $timespan = 60;

            if ($duration <= 90) {
                $rate = ExtractMultipleFramesFilter::FRAMERATE_EVERY_SEC;
                $timespan = 1;
            } elseif ($duration <= 180) {
                $rate = ExtractMultipleFramesFilter::FRAMERATE_EVERY_2SEC;
                $timespan = 2;
            } elseif ($duration <= 450) {
                $rate = ExtractMultipleFramesFilter::FRAMERATE_EVERY_5SEC;
                $timespan = 5;
            } elseif ($duration <= 900) {
                $rate = ExtractMultipleFramesFilter::FRAMERATE_EVERY_10SEC;
                $timespan = 10;
            } elseif ($duration <= 2700) {
                $rate = ExtractMultipleFramesFilter::FRAMERATE_EVERY_30SEC;
                $timespan = 30;
            }

            if (!file_exists($this->framePath.'/frame-2.jpg')) {
                $ffmpeg = FFMpeg::create(Module::getLoadedModule()->getFfmpegConfig());
                $video = $ffmpeg->open($fileName);

                $frameFileType = 'jpg';
                $filter = new ExtractMultipleFramesFilter($rate, $this->framePath);
                $filter->setFrameFileType($frameFileType);

                $video->addFilter($filter);

                $video->filters()->synchronize();

                $format = new X264('aac');

                if ($sourceFormat === 'flv') {
                    $format->setPasses(1);
                }

                $video->save($format, $this->framePath.'/trash.mp4');

                unlink($this->framePath.'/trash.mp4');
                unlink($this->framePath.'/frame-1.jpg');
            }

            $createVtt = $this->rawFileVttFormatName !== null && !file_exists($this->rawFileVttFormatName);
            $createVttImage = $this->rawFileVttImageFormatName !== null && !file_exists($this->rawFileVttImageFormatName);

            /**
             * Based on https://github.com/elalemanyo/thumbnail-webvtt by elalemanyo
             */
            if ($createVtt || $createVttImage) {
                $files = scandir($this->framePath);
                sort($files, SORT_STRING | SORT_FLAG_CASE | SORT_NATURAL);

                $files = array_values(array_filter($files, static function ($file) {
                    return StringHelper::endsWith($file, '.jpg');
                }));

                $total = count($files);

                if (true||$createVttImage) {
                    $thumbsAcross = min($total, 200);

                    $image = new \Imagick();
                    $image->readImage($this->framePath.'/'.$files[0]);
                    $thumbImageWidth = $image->getImageWidth();
                    $thumbImageHeight = $image->getImageHeight();
                    $image->clear();

                    $rows = ceil($total/$thumbsAcross);
                    $w = $thumbImageWidth * $thumbsAcross;
                    $h = $thumbImageHeight * $rows;

                    $coalesceImage = new \Imagick();
                    $coalesceImage->setSize($w, $h);
                }

                $vtt = 'WEBVTT'.PHP_EOL.PHP_EOL;
                $second = 0;
                $vttImageX = 0;
                $vttImageY = 0;

                foreach ($files as $fileIndex => $file) {
                    $t1 = sprintf('%02d:%02d:%02d.000', ($second / 3600), ($second / 60 % 60), $second % 60);
                    $second += $timespan;
                    $t2 = sprintf('%02d:%02d:%02d.000', ($second / 3600), ($second / 60 % 60), $second % 60);

                    if ($fileIndex && !($fileIndex % $thumbsAcross)) {
                        $vttImageX = 0;
                        ++$vttImageY;
                    }

                    if ($createVttImage) {
                        imagecopymerge($coalesce, imagecreatefromjpeg($this->framePath.'/'.$file), $vttImageX * $thumbImageWidth, $vttImageY * $thumbImageHeight, 0, 0, $thumbImageWidth, $thumbImageHeight, 100);
                    }

                    $vtt .= sprintf('%s --> %s\nfileNAME.jpg#xywh=%d,%d,%d,%d', $t1, $t2, $vttImageX++ * $thumbImageWidth, $vttImageY * $thumbImageHeight,  $thumbImageWidth, $thumbImageHeight);
                    $vtt .= PHP_EOL.PHP_EOL;
                }

                if ($createVttImage) {
                    imagejpeg($coalesce, $this->rawFileVttImageFormatName, 75);
                }

                if ($createVtt) {
                    file_put_contents($this->rawFileVttFormatName, $vtt);
                }
            }
        } catch (\Exception $e) {
            var_dump($e->getLine().': '.$e->getMessage());
        }
    }

    /**
     * @inheritdoc
     */
    public function getTtr()
    {
        return 3 * (60 * 60);
    }

    /**
     * @inheritdoc
     */
    public function canRetry($attempt, $error)
    {
        return $attempt < 3;
    }
}
