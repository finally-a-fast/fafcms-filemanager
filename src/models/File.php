<?php

namespace fafcms\filemanager\models;

use fafcms\filemanager\abstracts\models\BaseFile;
use fafcms\filemanager\Module;
use fafcms\fafcms\{inputs\SwitchCheckbox, inputs\DateTimePicker, inputs\DropDownList, inputs\ExtendedDropDownList, inputs\Textarea, inputs\TextInput, models\User};

use fafcms\filemanager\assets\MediaAsset;
use fafcms\filemanager\assets\VideoJsAsset;
use fafcms\filemanager\Bootstrap;
use fafcms\filemanager\controllers\FilemanagerController;
use fafcms\filemanager\items\FilePreview;
use fafcms\helpers\ActiveRecord;
use fafcms\helpers\interfaces\EditViewInterface;
use fafcms\helpers\interfaces\FieldConfigInterface;
use fafcms\helpers\traits\BeautifulModelTrait;
use fafcms\helpers\traits\TagTrait;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\validators\DateValidator;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * This is the model class for table "file".
 *
 * @property int $id
 * @property string $status
 * @property string $display_start
 * @property string $display_end
 * @property int $filegroup_id
 * @property int $filetype_id
 * @property string $filename
 * @property int $number
 * @property string $alt
 * @property int $size
 * @property string $meta
 * @property int $is_public
 * @property int $created_by
 * @property int $updated_by
 * @property int $activated_by
 * @property int $deactivated_by
 * @property int $deleted_by
 * @property string $created_at
 * @property string $updated_at
 * @property string $activated_at
 * @property string $deactivated_at
 * @property string $deleted_at
 *
 * @property Cart[] $carts
 * @property ConsumptionFile[] $consumptionFiles
 * @property User $activatedBy
 * @property User $createdBy
 * @property User $deactivatedBy
 * @property User $deletedBy
 * @property Filegroup $filegroup
 * @property Filetype $filetype
 * @property User $updatedBy
 */
class File extends BaseFile
{
    use TagTrait;

    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataUrl($model): string
    {
        return  '/' . Bootstrap::$id . '/file';
    }

    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return 'file-outline';
    }

    /**
     * @inheritDoc
     */
    public static function extendedLabel($model, bool $html = true, array $params = []): string
    {
        return trim(($model['filename'] ?? ''));
    }
    //endregion BeautifulModelTrait implementation

    //region FieldConfigInterface implementation
    public function getFieldConfig(): array
    {
        return [
            'filegroup_id' => [
                'type' => ExtendedDropDownList::class,
                'items' => static function() {
                    return Filegroup::instance()->getSelect(true)['items'];
                },
                'relationClassName' => Filegroup::class,
            ],
            'filegroup.name' => [
                'type' => TextInput::class,
            ],
            'filename' => [
                'type' => TextInput::class,
            ],
            'number' => [
                'type' => TextInput::class,
            ],
            'status' => [
                'type' => DropDownList::class,
                'items' => $this->attributeOptions()['status']
            ],
            'display_start' => [
                'type' => DateTimePicker::class,
            ],
            'display_end' => [
                'type' => DateTimePicker::class,
            ],
            'alt' => [
                'type' => Textarea::class,
                'counter' => true,
                'maxlength' => true
            ],
            'relativeUrl' => [
                'type' => TextInput::class,
                'showCopyButton' => true
            ],
            'absoluteUrl' => [
                'type' => TextInput::class,
                'showCopyButton' => true
            ],
            'is_public' => [
                'type' => SwitchCheckbox::class,
                'description' => Yii::t('fafcms-filemanager', 'Determines whether a login is required to open the file.'),
                'offLabel' => Yii::t('fafcms-core', 'No'),
                'onLabel' => Yii::t('fafcms-core', 'Yes')
            ]
        ];
    }
    //endregion FieldConfigInterface implementation

    //region EditViewInterface implementation
    public static function editView(): array
    {
        return [
            'default' => [
                'tab-1' => [
                    'class' => \fafcms\fafcms\items\Tab::class,
                    'settings' => [
                        'label' => Yii::t('fafcms-core', 'Master data'),
                    ],
                    'contents' => [
                        'row-1' => [
                            'class' => \fafcms\fafcms\items\Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => \fafcms\fafcms\items\Column::class,
                                    'settings' => [
                                        'm' => 6,
                                    ],
                                    'contents' => [
                                        'master-data-card' => [
                                            'class' => \fafcms\fafcms\items\Card::class,
                                            'settings' => [
                                                'title' => Yii::t('fafcms-core', 'Master data'),
                                                'icon' => 'id-card',
                                            ],
                                            'contents' => [
                                                [
                                                    'class' => \fafcms\fafcms\items\FormField::class,
                                                    'settings' => [
                                                        'field' => 'filegroup_id',
                                                    ],
                                                ],
                                                [
                                                    'class' => \fafcms\fafcms\items\FormField::class,
                                                    'settings' => [
                                                        'field' => 'filegroup.name',
                                                    ],
                                                ],
                                                [
                                                    'class' => \fafcms\fafcms\items\FormField::class,
                                                    'settings' => [
                                                        'field' => 'filename',
                                                    ],
                                                ],
                                                [
                                                    'class' => \fafcms\fafcms\items\FormField::class,
                                                    'settings' => [
                                                        'field' => 'number',
                                                    ],
                                                ],
                                            ]
                                        ],
                                    ]
                                ],
                                'column-2' => [
                                    'class' => \fafcms\fafcms\items\Column::class,
                                    'settings' => [
                                        'm' => 5,
                                    ],
                                    'contents' => [
                                        'access-restrictions-card' => [
                                            'class' => \fafcms\fafcms\items\Card::class,
                                            'settings' => [
                                                'title' => Yii::t('fafcms-core', 'Access restrictions'),
                                                'icon' => 'timetable',
                                            ],
                                            'contents' => [
                                                [
                                                    'class' => \fafcms\fafcms\items\FormField::class,
                                                    'settings' => [
                                                        'field' => 'status',
                                                    ],
                                                ],
                                                [
                                                    'class' => \fafcms\fafcms\items\FormField::class,
                                                    'settings' => [
                                                        'field' => 'display_start',
                                                    ],
                                                ],
                                                [
                                                    'class' => \fafcms\fafcms\items\FormField::class,
                                                    'settings' => [
                                                        'field' => 'display_end',
                                                    ],
                                                ],
                                            ]
                                        ],
                                    ]
                                ],
                                'column-3' => [
                                    'class' => \fafcms\fafcms\items\Column::class,
                                    'settings' => [
                                        'm' => 5,
                                    ],
                                    'contents' => [
                                        'access-restrictions-card' => [
                                            'class' => \fafcms\fafcms\items\Card::class,
                                            'settings' => [
                                                'title' => Yii::t('fafcms-core', 'Preview'),
                                                'icon' => 'file-eye-outline',
                                            ],
                                            'contents' => [
                                                [
                                                    'class' => FilePreview::class,
                                                    'settings' => [
                                                        'field' => 'id',
                                                    ],
                                                ],
                                            ]
                                        ],
                                    ]
                                ]
                            ],
                        ],
                        'row-2' => [
                            'class' => \fafcms\fafcms\items\Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => \fafcms\fafcms\items\Column::class,
                                    'settings' => [
                                        'm' => 8,
                                    ],
                                    'contents' => [
                                        'content-card' => [
                                            'class' => \fafcms\fafcms\items\Card::class,
                                            'settings' => [
                                                'title' => Yii::t('fafcms-filemanager', 'Web access'),
                                                'icon' => 'web',
                                            ],
                                            'contents' => [
                                                [
                                                    'class' => \fafcms\fafcms\items\FormField::class,
                                                    'settings' => [
                                                        'field' => 'is_public',
                                                    ],
                                                ],
                                                [
                                                    'class' => \fafcms\fafcms\items\FormField::class,
                                                    'settings' => [
                                                        'field' => 'relativeUrl',
                                                    ],
                                                ],
                                                [
                                                    'class' => \fafcms\fafcms\items\FormField::class,
                                                    'settings' => [
                                                        'field' => 'absoluteUrl',
                                                    ],
                                                ],
                                            ]
                                        ],
                                    ]
                                ],
                                'column-2' => [
                                    'class' => \fafcms\fafcms\items\Column::class,
                                    'settings' => [
                                        'm' => 8,
                                    ],
                                    'contents' => [
                                        'content-card' => [
                                            'class' => \fafcms\fafcms\items\Card::class,
                                            'settings' => [
                                                'title' => Yii::t('fafcms-filemanager', 'Alternative text'),
                                                'icon' => 'card-text-outline',
                                            ],
                                            'contents' => [
                                                [
                                                    'class' => \fafcms\fafcms\items\FormField::class,
                                                    'settings' => [
                                                        'field' => 'alt',
                                                        'hideLabel' => true
                                                    ],
                                                ],
                                            ]
                                        ],
                                    ]
                                ],
                            ],
                        ]
                    ]
                ],
            ],
        ];
    }
    //endregion EditViewInterface implementation

    public $slug;

    public static function getUrl($file, $scheme = false, $type = [], $params = [])
    {
        if ($type === null) {
            $type = [];
        }

        if (count($type) > 0) {
            $type = Yii::$app->security->hashData(base64_encode(json_encode($type)), Yii::$app->getModule(Bootstrap::$id)->validationKey);
        }

        $params[0] = (Yii::$app->fafcms->getIsBackend() ? '/' . Yii::$app->fafcms->getBackendUrl() : '') . '/' . Bootstrap::$id . '/filemanager/get-file';
        $params['id'] = $file['id'];
        $params['type'] = $type;

        return Url::to($params, $scheme);
    }

    public function getRelativeUrl()
    {
        return self::getUrl($this);
    }

    public function getAbsoluteUrl()
    {
        return self::getUrl($this, true);
    }

    public static function unitToInt($s)
    {
        return (int)preg_replace_callback('/(\-?\d+)(.?)/', function ($m) {
            return $m[1] * pow(1024, strpos('BKMG', $m[2]));
        }, strtoupper($s));
    }

    public static function getPicture($file, $name, $rawImageFormats, $responsive = true, $loader = null, $options = [], $wrapperOptions = [], $defaultOptions = '')
    {
        $path = self::getFilePath($file);

        if (!file_exists($path)) {
            return '';
        }

        if (($file['size']??filesize($path)) > self::unitToInt(ini_get('memory_limit'))) {
            return false;
        }

        if (!($file instanceof File)) {
            $file = self::find()->where(['id' => $file['id']])->with('filetype')->one();
        }

        $fileMediatype = $file['filetype']['mediatype'];

        if ($fileMediatype === 'video') {
            if ($defaultOptions === '' ||
                empty($defaultOptionArray = Html::cssStyleToArray($defaultOptions)) ||
                !isset($defaultOptionArray['type']) ||
                ($defaultOptionArray['type'] !== 'animation' && $defaultOptionArray['type'] !== 'thumbnail')
            ) {
                return false;
            }

            if ($defaultOptionArray['type'] === 'thumbnail') {
                FilemanagerController::getRawImageFromVideo($file, $options);
            }

            if ($defaultOptionArray['type'] === 'animation') {
                FilemanagerController::getRawAnimationFromVideo($file, $options);
            }
        }

        $meta = self::loadMeta($file);

        if (!isset($meta['width'], $meta['height'])) {
            return false;
        }

        $originalWidth = $meta['width'];
        $originalHeight = $meta['height'];
        $aspect = $originalWidth / $originalHeight;

        if (count($rawImageFormats) === 0) {
            $rawImageFormats[] = [$originalWidth, $originalHeight];
        }

        $sizes = ['default' => [], 'webp' => []];
        $webp = Yii::$app->getModule(Bootstrap::$id)->webp;

        $minImage = null;
        $minImagePosition = null;
        $minImageFormat = ['width' => PHP_INT_MAX, 'height' => PHP_INT_MAX];
        $minImagePositionMedia = null;

        foreach ($rawImageFormats as $rawImageFormat) {
            $imageFormat = self::getImageFormat($rawImageFormat, $aspect, $defaultOptions);

            if ($imageFormat === false) {
                continue;
            }

            $image = self::getUrl($file, false, [$name, $imageFormat['width'], $imageFormat['height'], $imageFormat['options']]);
            $media = Html::cssStyleToArray($imageFormat['options'])['media']??'all';
            $sizes['default'][$media][] = [
                'src' => $image,
                'width' => $imageFormat['width'] . 'w'
            ];

            if ($imageFormat['width'] < $minImageFormat['width'] || ($imageFormat['width'] === $minImageFormat['width'] && $imageFormat['height'] < $minImageFormat['height'])) {
                $minImage = $image;
                $minImagePositionMedia = $media;
                $minImagePosition = count($sizes['default'][$media]) - 1;
                $minImageFormat = $imageFormat;
            }

            if ($webp) {
                $imageFormatOptions = [
                    'style' => $imageFormat['options']
                ];

                Html::addCssStyle($imageFormatOptions, [
                    'format' => 'webp',
                ]);

                $imageFormat['options'] = $imageFormatOptions['style'];

                $sizes['webp'][$media][] = [
                    'src' => self::getUrl($file, false, [$name, $imageFormat['width'], $imageFormat['height'], $imageFormat['options']]),
                    'width' => $imageFormat['width'].'w'
                ];
            }
        }

        $defaultImage = null;

        if ($loader === false) {
            $defaultImage = $minImage;
            if ($minImagePositionMedia === 'all') {
                array_splice($sizes['default']['all'], $minImagePosition, 1);
            }
        } else {
            if ($loader !== null && $loader !== '') {
                $imageFormat = self::getImageFormat($loader, $aspect, $defaultOptions);

                if ($imageFormat !== false) {
                    $defaultImage = self::getUrl($file, false, [$name, $imageFormat['width'], $imageFormat['height'], $imageFormat['options']]);
                }
            }

            if ($defaultImage === null) {
                $imageFormat = $minImageFormat;

                $loaderOptions = [
                    'style' => $imageFormat['options']??[]
                ];

                Html::addCssStyle($loaderOptions, [
                    'quality' => 40,
                    'blur' => 'true',
                    'blur-radius' => 15,
                    'blur-sigma' => 10,
                ]);

                $imageFormat['options'] = $loaderOptions['style'];
                $defaultImage = self::getUrl($file, false, [$name, $imageFormat['width'], $imageFormat['height'], $imageFormat['options']]);
            }
        }

        if (count($sizes['default']) > 0) {
            Html::addCssClass($options, 'lazyload');
            $options['data-sizes'] = 'auto';
        }

        $pictureOptions = [];

        if ($responsive) {
            $minImageFormatWidth = $minImageFormat['width'];
            $minImageFormatHeight = $minImageFormat['height'];

            if ((Html::cssStyleToArray($minImageFormat['options'])['bestfit']??'false') === 'true') {
                if ($aspect > 1) {
                    $minImageFormatHeight = floor($minImageFormatWidth / $aspect);
                } else {
                    $minImageFormatWidth = floor($minImageFormatHeight * $aspect);
                }
            }

            Html::addCssStyle($pictureOptions, [
                'padding-bottom' => (($minImageFormatHeight / $minImageFormatWidth) * 100).'%'
            ]);

            Html::addCssClass($pictureOptions, 'responsive-picture');
        }

        $sources = '';

        if ($webp) {
            foreach ($sizes['webp'] as $media => $mediaSizes) {
                if (count($mediaSizes) === 1) {
                    $srcset = $mediaSizes[0]['src'];
                } else {
                    $srcset = '';

                    foreach ($mediaSizes as $size) {
                        $srcset .= ($srcset !== '' ? ', ' : '') . $size['src'] . ' ' . $size['width'];
                    }
                }

                $sources .= '<source' . Html::renderTagAttributes(['data-srcset' => $srcset, 'type' => 'image/webp', 'media' => $media]) . ' />';
            }
        }

        $defaultSrcset = '';

        if (isset($sizes['default'])) {
            foreach ($sizes['default'] as $media => $mediaSizes) {
                if (count($mediaSizes) === 1) {
                    $srcset = $mediaSizes[0]['src'];
                } else {
                    $srcset = '';

                    foreach ($mediaSizes as $size) {
                        $srcset .= ($srcset !== '' ? ', ' : '') . $size['src'] . ' ' . $size['width'];
                    }
                }

                if ($media === 'all') {
                    $defaultSrcset = $srcset;
                } else {
                    $sources .= '<source' . Html::renderTagAttributes(['data-srcset' => $srcset, 'media' => $media]) . ' />';
                }
            }
        }

        if ($defaultSrcset !== '') {
            $options['data-srcset'] = $defaultSrcset;
        }

        if (!isset($options['alt']) && !empty($file['alt'])) {
            $options['alt'] = $file['alt'];
        }

        MediaAsset::register(Yii::$app->view);

        Html::addCssClass($wrapperOptions, 'fafcms-filemanager-image-wrapper');
        return Html::tag('div', Html::tag('picture', $sources .  Html::img($defaultImage, $options), $pictureOptions), $wrapperOptions);
    }

    public static function getVideo($file, $name, $rawVideoFormats = [], $responsive = true, $poster = null, $options = [], $wrapperOptions = [])
    {
        if (count($rawVideoFormats) === 0) {
            $rawVideoFormats[] = ['raw'];
        }

        $path = self::getFilePath($file);

        if (!file_exists($path)) {
            return '';
        }

        $setup = [
            'sources' => [],
            'fluid' => $responsive,
        ];

        foreach ($rawVideoFormats as $rawVideoFormat) {
            if ($rawVideoFormat[0]??['raw'] === 'raw') {
                $setup['sources'][] = [
                    'type' => $file['mime_type']??$file['filetype']['mime_type'],
                    'src' => self::getUrl($file, false)
                ];
            }
        }

        if ($poster !== null) {
            $setup['poster'] = $poster;
        }

        VideoJsAsset::register(Yii::$app->view);

        Html::addCssClass($wrapperOptions, 'fafcms-filemanager-video-wrapper');
        Html::addCssClass($options, 'video-js vjs-default-skin vjs-big-play-centered');

        $options['controls'] = true;
        $options['data-setup'] = $setup;

        return Html::tag('div', Html::tag('video', '', $options), $wrapperOptions);
    }

    public static function getImageFormat($rawImageFormat, $aspect, $defaultOptions = '')
    {
        $width = $rawImageFormat[0] ?? null;
        $height = $rawImageFormat[1] ?? null;

        if (($width === null || $width === '') && ($height === null || $height === '')) {
            return false;
        }

        if ($width === null || $width === '') {
            $width = floor($height * $aspect);
        } elseif ($height === null || $height === '') {
            $height = floor($width / $aspect);
        }

        $options = $rawImageFormat[2] ?? '';

        if ($defaultOptions !== '') {
            if ($options !== '') {
                $options .= ';';
            }

            $options .= $defaultOptions;
        }

        return [
            'width' => $width,
            'height' => $height,
            'options' => $options
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function prefixableTableName(): string
    {
        return '{{%file}}';
    }

    public function behaviors(): array
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'sluggable' => [
                'class'           => SluggableBehavior::class,
                'attribute'       => 'filename',
                'ensureUnique'    => true,
                'slugAttribute'   => 'filename',
                'uniqueValidator' => [
                    'targetAttribute' => ['filegroup_id', 'filename'],
                ],
            ],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            ['display_start', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'display_start', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            ['display_end', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'display_end', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            [['display_start', 'display_end', 'created_at', 'updated_at', 'activated_at', 'deactivated_at', 'deleted_at'], 'safe'],
            [['filegroup_id', 'filetype_id', 'filename', 'size'], 'required'],
            [['filegroup_id', 'filetype_id', 'number', 'size', 'is_public', 'created_by', 'updated_by', 'activated_by', 'deactivated_by', 'deleted_by'], 'integer'],
            [['status', 'filename', 'alt'], 'string', 'max' => 255],
            [['meta'], 'string'],
            [['filename', 'filegroup_id'], 'unique', 'targetAttribute' => ['filename', 'filegroup_id'], 'message' => Yii::t('fafcms-filemanager', 'File with same name already exists in this folder.'), 'on' => [self::SCENARIO_UPDATE]],
            [['activated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['activated_by' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['created_by' => 'id']],
            [['deactivated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['deactivated_by' => 'id']],
            [['deleted_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['deleted_by' => 'id']],
            [['filegroup_id'], 'exist', 'skipOnError' => true, 'targetClass' => Filegroup::class, 'targetAttribute' => ['filegroup_id' => 'id'], 'when' => static function ($model) {
                return (int)$model->filegroup_id !== 0;
            }],
            [['filetype_id'], 'exist', 'skipOnError' => true, 'targetClass' => Filetype::className(), 'targetAttribute' => ['filetype_id' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['updated_by' => 'id']],
        ]);
    }

    public static function getFileName($file, $name = null, $withOutExtension = false)
    {
        if (isset($file['default_extension'])) {
            $extension = $file['default_extension'];
        } else {
            $cacheName = 'fafcms-filemanager-filetype-' . $file['filetype_id'];
            $filetype = Yii::$app->cache->get($cacheName);

            if ($filetype === false) {
                $filetype = Filetype::find()->where(['id' => $file['filetype_id']])->asArray()->one();
                Yii::$app->cache->set($cacheName, $filetype, null);
            }

            $extension = $filetype['default_extension'];
        }

        return ($name ?? $file['filename']) . ($withOutExtension ? '' : '.' . $extension);
    }

    public static function getFileTarget($file)
    {
        return Filegroup::getPath($file['filegroup_id']);
    }

    public static function getFilePath($file, $name = null, $withOutExtension = false)
    {
        return self::getFileTarget($file) . '/' . self::getFileName($file, $name, $withOutExtension);
    }

    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }

        foreach (glob(self::getFilePath($this, $this['filename'] . '-FAFCMS-GENERATED-*', true)) as $file) {
            try {
                if (file_exists($file) && !unlink($file)) {
                    $this->addError('file', 'Cannot delete '.$file.'.<br>Please check the file permissions!');
                    return false;
                }
            } catch (\Exception $e) {
                $this->addError('file', 'Cannot delete '.$file.'.<br>Please check the file permissions!');
                return false;
            }
        }

        $filename = self::getFilePath($this);

        try {
            if (file_exists($filename) && !unlink($filename)) {
                $this->addError('file', 'Cannot delete '.$filename.'.<br>Please check the file permissions!');
                return false;
            }
        } catch (\Exception $e) {
            $this->addError('file', 'Cannot delete '.$filename.'.<br>Please check the file permissions!');
            return false;
        }

        return true;
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert): bool
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        if ((int)$this->filegroup_id === 0) {
            $this->filegroup_id = null;
        }

        if (!$insert) {
            if ($this->isAttributeChanged('filename') || $this->isAttributeChanged('filegroup_id')) {
                @rename(self::getFilePath($this->oldAttributes), self::getFilePath($this->attributes));
            }

            foreach (glob(self::getFilePath($this->oldAttributes, $this->oldAttributes['filename'].'-FAFCMS-GENERATED-*')) as $file) {
                @rename($file, str_replace(self::getFileTarget($this->oldAttributes).'/'.$this->oldAttributes['filename'], self::getFileTarget($this->attributes).'/'.$this->attributes['filename'], $file));
            }
        }

        if ($this->number === null) {
            $stepSize = (int)Yii::$app->getModule(Bootstrap::$id)->getPluginSettingValue('file_number_step_size');
            $startValue = (int)Yii::$app->getModule(Bootstrap::$id)->getPluginSettingValue('file_number_start_value');

            $this->number = new Expression('IFNULL(('.self::find()->select('MAX(number) + '.$stepSize)->alias('file_number')->createCommand()->getRawSql().'), '.$startValue.')');
        }

        return true;
    }

    /**
     * @var null|array
     */
    private $_loadedMeta = null;

    /**
     * @param File       $model
     * @param array|null $fileVariation
     *
     * @return array
     * @throws \ImagickException
     * @throws \yii\base\ErrorException
     */
    public static function loadMeta(File $model, array $fileVariation = null): array
    {
        $id = 'original';

        if ($fileVariation !== null) {
            $id = $fileVariation['filesizevariation']->id . '-' . $fileVariation['fileformatvariation']->id;
        }

        if ($model->_loadedMeta === null) {
            if ($model->meta === null) {
                $model->_loadedMeta = ['variation' => []];
            } else {
                $model->_loadedMeta = @unserialize($model->meta, ['allowed_classes' => false]);
            }
        }

        $version = $model->_loadedMeta['version'] ?? 1;
        $currentVersion = 2;

        if ($version < $currentVersion) {
            $model->_loadedMeta = null;
        }

        $meta = $model->_loadedMeta['variation'][$id] ?? null;

        if ($meta === null) {
            $meta = [];
            $mediatype = $model->filetype->mediatype;
            $defaultExtension = $model->filetype->default_extension;
            $meta['filetypeId'] = $model->filetype->id;
            $meta['size'] = $model->size;

            if ($fileVariation === null) {
                $path = self::getFilePath($model);

                if (!file_exists($path)) {
                    return [];
                }
            } else {
                $path = $fileVariation['name'];

                if (!file_exists($path)) {
                    return [];
                }

                $filetypesByMime = ArrayHelper::index(Filetype::find()->asArray()->all(), 'mime_type');
                $mimeType = FileHelper::getMimeTypeByExtension($path);

                if (isset($filetypesByMime[$mimeType])) {
                    $mediatype = $filetypesByMime[$mimeType]['mediatype'];
                    $defaultExtension = $filetypesByMime[$mimeType]['default_extension'];
                    $meta['filetypeId'] = $filetypesByMime[$mimeType]['id'];
                    $meta['size'] = filesize($path);
                }
            }

            if ($mediatype === 'video') {
                $ffprobe = \FFMpeg\FFProbe::create(Module::getLoadedModule()->getFfmpegConfig());
                $meta['duration'] = $ffprobe->format($path)->get('duration');

                $dimensions = $ffprobe->streams($path)->videos()->first()->getDimensions();

                $meta['width'] = $dimensions->getWidth();
                $meta['height'] = $dimensions->getHeight();
            } else {
                if ($defaultExtension === 'pdf') {
                    $image = new \Imagick();
                    $image->readImage($path);

                    try {
                        $meta['width'] = $image->getImageWidth();
                        $meta['height'] = $image->getImageHeight();
                    } catch (\ImagickException $e) {}

                    $meta['pageCount'] = $image->getNumberImages();
                }

                try {
                    if ($mediatype === 'image') {
                        $image = new \Imagick();
                        $image->readImage($path);

                        $meta['width'] = $image->getImageWidth();
                        $meta['height'] = $image->getImageHeight();

                        /*try {
                            $meta['exif'] = exif_read_data($path, 0, true);
                        } catch (\Exception $e) {
                            //TODO log exception
                        }*/
                    } elseif ($defaultExtension === 'pdf') {
                        $image = new \Imagick();
                        $image->readImage($path);

                        try {
                            $meta['width'] = $image->getImageWidth();
                            $meta['height'] = $image->getImageHeight();
                        } catch (\ImagickException $e) {}

                        $meta['pageCount'] = $image->getNumberImages();
                    }
                } catch (\ImagickException $e) {
                    //TODO log exception
                }
            }

            $model->_loadedMeta['variation'][$id] = $meta;
            $model->_loadedMeta['version'] = $currentVersion;

            $model->meta = serialize($model->_loadedMeta);
            $model->save();
        }

        return $meta;
    }

    /**
     * @param array|null $fileVariation
     * @return array
     */
    public function getLoadMeta(array $fileVariation = null): array
    {
        return self::loadMeta($this, $fileVariation);
    }

    /**
     * @param File $model
     * @param array|null $fileVariation
     * @return string
     */
    public static function formatedMeta(File $model, array $fileVariation = null): string
    {
        $filetype = $model->filetype;

        if ((int)$model->size === 0) {
            $path = self::getFilePath($model);
            $model->size = filesize($path);
            $model->save();
        }

        if ($fileVariation !== null) {
            $meta = self::loadMeta($model, $fileVariation);
            $filetype = Filetype::find()->where(['id' => $meta['filetypeId']])->one();

            if ($filetype === null) {
                return '';
            }
        }

        $metaString = '';

        $formatedMeta = [self::formatedMetaSize($model, $fileVariation)];
        $formatedMeta[] = self::formatedMetaType($model, $fileVariation);

        if ($filetype->mediatype === 'image') {
            $formatedMeta[] = self::formatedMetaWidth($model, $fileVariation);
            $formatedMeta[] = self::formatedMetaHeight($model, $fileVariation);
            //$metaString .= self::formatedMetaExif($model, $fileVariation);
        } elseif ($filetype->default_extension === 'pdf') {
            $formatedMeta[] = self::formatedMetaPageCount($model, $fileVariation);
        }

        return implode(' | ', array_filter($formatedMeta)).$metaString;
    }

    /**
     * @param array|null $fileVariation
     * @return string
     */
    public function getFormatedMeta(array $fileVariation = null): string
    {
        return self::formatedMeta($this, $fileVariation);
    }

    /**
     * @param File $model
     * @param array|null $fileVariation
     * @return string
     */
    public static function formatedMetaSize(File $model, array $fileVariation = null): string
    {
        $size = $model->size;

        if ($fileVariation !== null) {
            $meta = self::loadMeta($model, $fileVariation);
            $size = $meta['size'];
        }

        return Yii::t('fafcms-filemanager', 'Size: {size}', ['size' => Yii::$app->getFormatter()->asShortSize($size, 2)]);
    }

    /**
     * @param array|null $fileVariation
     * @return string
     */
    public function getFormatedMetaSize(array $fileVariation = null): string
    {
        return self::formatedMetaSize($this, $fileVariation);
    }

    /**
     * @param File $model
     * @param array|null $fileVariation
     * @return string
     */
    public static function formatedMetaType(File $model, array $fileVariation = null): string
    {
        return Yii::t('fafcms-filemanager', 'Type: {value}', ['value' => self::formatedMetaTypeNoLabel($model, $fileVariation)]);
    }

    /**
     * @param array|null $fileVariation
     * @return string
     */
    public function getFormatedMetaType(array $fileVariation = null): string
    {
        return self::formatedMetaType($this, $fileVariation);
    }

    //TODO provide other meta data also without label
    /**
     * @param File $model
     * @param array|null $fileVariation
     * @return string
     */
    public static function formatedMetaTypeNoLabel(File $model, array $fileVariation = null): string
    {
        $filetype = $model->filetype;

        if ($fileVariation !== null) {
            $meta = self::loadMeta($model, $fileVariation);
            $filetype = Filetype::find()->where(['id' => $meta['filetypeId']])->one();

            if ($filetype === null) {
                return '';
            }
        }

        return Yii::t('fafcms-filemanager', $filetype->name).' (.'.$filetype->default_extension.')';
    }

    /**
     * @param array|null $fileVariation
     * @return string
     */
    public function getFormatedMetaTypeNoLabel(array $fileVariation = null): string
    {
        return self::formatedMetaTypeNoLabel($this, $fileVariation);
    }

    /**
     * @param File $model
     * @param array|null $fileVariation
     * @return bool|string
     */
    public static function formatedMetaWidth(File $model, array $fileVariation = null)
    {
        $meta = self::loadMeta($model, $fileVariation);

        if (isset($meta['width'])) {
            return Yii::t('fafcms-filemanager', 'Width: {width}', ['width' => $meta['width'].'px']);
        }

        return false;
    }

    /**
     * @param array|null $fileVariation
     * @return bool|string
     */
    public function getFormatedMetaWidth(array $fileVariation = null)
    {
        return self::formatedMetaWidth($this, $fileVariation);
    }

    /**
     * @param File $model
     * @param array|null $fileVariation
     * @return bool|string
     */
    public static function formatedMetaHeight(File $model, array $fileVariation = null)
    {
        $meta = self::loadMeta($model, $fileVariation);

        if (isset($meta['height'])) {
            return Yii::t('fafcms-filemanager', 'Height: {height}', ['height' =>  $meta['height'].'px']);
        }

        return false;
    }

    /**
     * @param array|null $fileVariation
     * @return bool|string
     */
    public function getFormatedMetaHeight(array $fileVariation = null)
    {
        return self::formatedMetaHeight($this, $fileVariation);
    }

    /**
     * @param File $model
     * @param array|null $fileVariation
     * @return bool|string
     */
    public static function formatedMetaExif(File $model, array $fileVariation = null)
    {
        $meta = self::loadMeta($model, $fileVariation);

        if (isset($meta['exif'])) {
            return Html::tag('div', '', ['exif-data' => $meta['exif']]);
        }

        return false;
    }

    /**
     * @param array|null $fileVariation
     * @return bool|string
     */
    public function getFormatedMetaExif(array $fileVariation = null)
    {
        return self::formatedMetaExif($this, $fileVariation);
    }

    /**
     * @param File $model
     * @param array|null $fileVariation
     * @return bool|string
     */
    public static function formatedMetaPageCount(File $model, array $fileVariation = null)
    {
        $meta = self::loadMeta($model, $fileVariation);

        if (isset($meta['pageCount'])) {
            return Yii::t('fafcms-filemanager', 'Number of pages: {count}', ['count' => $meta['pageCount']]);
        }

        return false;
    }

    /**
     * @param array|null $fileVariation
     * @return bool|string
     */
    public function getFormatedMetaPageCount(array $fileVariation = null)
    {
        return self::formatedMetaPageCount($this, $fileVariation);
    }

    /**
     * @param File $file
     * @return string
     */
    public static function pdfUrl(File $file): string
    {
        return Url::to(['/'.Bootstrap::$id.'/filemanager/view-pdf', 'id' => $file['id'], '#' => 'zoom=page-fit']);
    }

    /**
     * @return string
     */
    public function getPdfUrl(): string
    {
        return self::pdfUrl($this);
    }
}
