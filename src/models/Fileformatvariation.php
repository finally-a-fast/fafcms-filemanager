<?php

namespace fafcms\filemanager\models;

use fafcms\filemanager\abstracts\models\BaseFileformatvariation;

/**
 * This is the model class for table "{{%fileformatvariation}}".
 *
 * @package fafcms\filemanager\models
 */
class Fileformatvariation extends BaseFileformatvariation
{

}
