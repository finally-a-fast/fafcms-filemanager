<?php
namespace fafcms\filemanager\models;

use yii\base\Model;

class FileSelectForm extends Model
{
    public $id;
    public $file;
    public $mediatypes;
    public $mimetypes;
    public ?int $limit = null;
    public $external;
    public ?int $filegroup = null;

    public function rules(): array
    {
        return [
            [['file', 'id'], 'string'],
            [['mediatypes'], 'each', 'rule' => ['in', 'range' => Filetype::find()->select('mediatype')->groupBy('mediatype')->column()]],
            [['mimetypes'], 'each', 'rule' => ['in', 'range' => Filetype::find()->select('mime_type')->groupBy('mime_type')->column()]],
            [['external'], 'boolean'],
            [['limit', 'filegroup'], 'integer'],
            [['filegroup'], 'in', 'range' => Filegroup::find()->select('id')->column()],
        ];
    }
}
