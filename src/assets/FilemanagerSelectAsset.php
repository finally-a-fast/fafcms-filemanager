<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 - 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-filemanager/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-filemanager
 * @see https://www.finally-a-fast.com/packages/fafcms-filemanager/docs Documentation of fafcms-filemanager
 * @since File available since Release 1.0.0
 */

declare(strict_types=1);

namespace fafcms\filemanager\assets;

use fafcms\fafcms\assets\fafcms\backend\FafcmsAjaxModalAsset;
use fafcms\fafcms\assets\fafcms\backend\FafcmsToastAsset;
use fafcms\helpers\classes\AssetComponentBundle;

/**
 * Class FilemanagerSelectAsset
 *
 * @package fafcms\filemanager\assets
 */
class FilemanagerSelectAsset extends AssetComponentBundle
{
    public $sourcePath = __DIR__ . '/filemanager-select';

    public $js = [
        'filemanager-select.js',
    ];

    public $css = [
        'filemanager-select.scss',
    ];

    public $depends = [
        FafcmsAjaxModalAsset::class,
        FafcmsToastAsset::class,
    ];
}
