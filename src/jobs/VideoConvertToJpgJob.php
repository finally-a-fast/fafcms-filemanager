<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-filemanager/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-filemanager
 * @see https://www.finally-a-fast.com/packages/fafcms-module-filemanager/docs Documentation of fafcms-module-filemanager
 * @since File available since Release 1.0.0
 */

namespace fafcms\filemanager\jobs;

use fafcms\filemanager\models\File;
use fafcms\filemanager\models\Filetype;
use fafcms\filemanager\Module;
use FFMpeg\Coordinate\TimeCode;
use FFMpeg\FFMpeg;

/**
 * Class VideoConvertToJpgJob
 * @package fafcms\filemanager\jobs
 */
class VideoConvertToJpgJob extends \yii\base\BaseObject implements \yii\queue\RetryableJobInterface
{
    public $fileId;
    public $rawFileFormatName;
    public $timeCode = 2;

    /**
     * @inheritdoc
     */
    public function execute($queue)
    {
        $file = File::find()->select([
                File::tableName().'.id',
                File::tableName().'.allow_download',
                File::tableName().'.is_public',
                File::tableName().'.filegroup_id',
                File::tableName().'.filetype_id',
                File::tableName().'.filename',
                Filetype::tableName().'.mediatype',
                Filetype::tableName().'.mime_type',
                Filetype::tableName().'.default_extension'
            ])
            ->innerJoinWith('filetype', false)
            ->where([
                File::tableName().'.id' => $this->fileId
            ])
            ->asArray()
            ->one();

        $fileName = File::getFilePath($file);

        $ffmpeg = FFMpeg::create(Module::getLoadedModule()->getFfmpegConfig());
        $video = $ffmpeg->open($fileName);
        $video->frame(TimeCode::fromSeconds($this->timeCode))->save($this->rawFileFormatName);
    }

    /**
     * @inheritdoc
     */
    public function getTtr()
    {
        return 3 * (60 * 60);
    }

    /**
     * @inheritdoc
     */
    public function canRetry($attempt, $error)
    {
        return $attempt < 3;
    }
}
