<?php
/**
 * @author    Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 Finally a fast
 * @license   https://www.finally-a-fast.com/packages/fafcms-module-filemanager/license MIT
 * @link      https://www.finally-a-fast.com/packages/fafcms-module-filemanager
 * @see       https://www.finally-a-fast.com/packages/fafcms-module-filemanager/docs Documentation of fafcms-module-filemanager
 * @since     File available since Release 1.0.0
 */

namespace fafcms\filemanager\models;

use fafcms\filemanager\{
    Bootstrap,
    inputs\FileUpload,
    Module
};

use fafcms\fafcms\{
    inputs\DateTimePicker,
    inputs\DropDownList,
    inputs\ExtendedDropDownList,
    inputs\SwitchCheckbox,
    inputs\TextInput,
    items\Card,
    items\Column,
    items\FormField,
    items\Row,
    items\Tab
};

use fafcms\helpers\{interfaces\EditViewInterface, interfaces\FieldConfigInterface, Model, traits\BeautifulModelTrait, traits\TagTrait};

use fafcms\parser\component\Parser;
use Yii;

use yii\{
    base\ErrorException,
    base\Exception,
    helpers\ArrayHelper,
    helpers\FileHelper,
    web\NotAcceptableHttpException,
    web\UploadedFile
};

/**
 * Class FileUploadForm
 *
 * @package fafcms\filemanager\models
 *
 * @property array $formOptions
 * @property array $fieldConfig
 */
class FileUploadForm extends Model implements FieldConfigInterface, EditViewInterface
{
    use BeautifulModelTrait;
    use TagTrait;

    public static ?string $authModelClass = File::class;

    /**
     * @var null|UploadedFile[]
     */
    public $files;
    public $filegroup_id;
    public $is_public;
    public $filetypes = [];
    public $maxFiles;
    public $maxFileSize;
    public $extensions = [];
    public $create_new_folder;
    public $new_folder;
    public $status;
    public $display_start;
    public $display_end;

    //region BeautifulModelTrait implementation
    /**
     * {@inheritdoc}
     */
    public static function editDataUrl(): string
    {
        return  '/' . Bootstrap::$id . '/file';
    }

    /**
     * {@inheritdoc}
     */
    public static function editDataIcon(): string
    {
        return 'file-outline';
    }

    /**
     * {@inheritdoc}
     */
    public static function editDataPlural(): string
    {
        return Yii::t('fafcms-filemanager', 'Files');
    }

    /**
     * {@inheritdoc}
     */
    public static function editDataSingular(): string
    {
        return Yii::t('fafcms-filemanager', 'File');
    }

    /**
     * {@inheritdoc}
     */
    public static function extendedLabel($model): string
    {
        return $model->files[0]->name??'';
    }
    //endregion BeautifulModelTrait implementation

    //region FieldConfigInterface implementation
    public function getFieldConfig(): array
    {
        return [
            'filegroup_id' => [
                'type' => ExtendedDropDownList::class,
                'items' => static function() {
                    return Filegroup::instance()->getSelect(true)['items'];
                },
                'relationClassName' => Filegroup::class,
            ],
            'create_new_folder' => [
                'type' => SwitchCheckbox::class,
                'offLabel' => ['fafcms-core', 'No'],
                'onLabel' => ['fafcms-core', 'Yes']
            ],
            'new_folder' => [
                'type' => TextInput::class
            ],
            'files' => [
                'type' => FileUpload::class,
                'maxFiles' => $this->maxFiles,
                'maxFileSize' => $this->maxFileSize,
                'acceptedFileExtensions' => $this->extensions,
            ],
            'status' => [
                'type' => DropDownList::class,
                'items' => $this->attributeOptions()['status']
            ],
            'display_start' => [
                'type' => DateTimePicker::class,
            ],
            'display_end' => [
                'type' => DateTimePicker::class,
            ],
            'is_public' => [
                'type' => SwitchCheckbox::class,
                'description' => Yii::t('fafcms-filemanager', 'Determines whether a login is required to open the file.'),
                'offLabel' => Yii::t('fafcms-core', 'No'),
                'onLabel' => Yii::t('fafcms-core', 'Yes')
            ]
        ];
    }
    //endregion FieldConfigInterface implementation

    //region EditViewInterface implementation
    public static function editView(): array
    {
        return [
            'default' => [
                'buttons' => [
                    static function($buttons, $model, $form, $editView) {
                        $buttons['save']['label'] = ['fafcms-filemanager', 'Upload'];
                        $buttons['save']['icon'] = 'upload-outline';
                        return $buttons;
                    }
                ],
                'contents' => [
                    'tab-1' => [
                        'class' => Tab::class,
                        'settings' => [
                            'label' => ['fafcms-filemanager', 'Upload'],
                        ],
                        'contents' => [
                            'row-1' => [
                                'class' => Row::class,
                                'contents' => [
                                    'column-1' => [
                                        'class' => Column::class,
                                        'settings' => [
                                            'm' => 6,
                                        ],
                                        'contents' => [
                                            'master-data-card' => [
                                                'class' => Card::class,
                                                'settings' => [
                                                    'title' => Yii::t('fafcms-filemanager', 'Destination folder'),
                                                    'icon' => 'folder-outline',
                                                ],
                                                'contents' => [
                                                    [
                                                        'class' => FormField::class,
                                                        'settings' => [
                                                            'field' => 'filegroup_id',
                                                        ],
                                                    ],
                                                    [
                                                        'class' => FormField::class,
                                                        'settings' => [
                                                            'field' => 'create_new_folder',
                                                        ],
                                                    ],
                                                    [
                                                        'class' => FormField::class,
                                                        'settings' => [
                                                            'field' => 'new_folder',
                                                        ],
                                                    ],
                                                ]
                                            ],
                                        ]
                                    ],
                                    'column-2' => [
                                        'class' => Column::class,
                                        'settings' => [
                                            'm' => 5,
                                        ],
                                        'contents' => [
                                            'access-restrictions-card' => [
                                                'class' => Card::class,
                                                'settings' => [
                                                    'title' => Yii::t('fafcms-core', 'Access restrictions'),
                                                    'icon' => 'timetable',
                                                ],
                                                'contents' => [
                                                    [
                                                        'class' => FormField::class,
                                                        'settings' => [
                                                            'field' => 'status',
                                                        ],
                                                    ],
                                                    [
                                                        'class' => FormField::class,
                                                        'settings' => [
                                                            'field' => 'display_start',
                                                        ],
                                                    ],
                                                    [
                                                        'class' => FormField::class,
                                                        'settings' => [
                                                            'field' => 'display_end',
                                                        ],
                                                    ],
                                                ]
                                            ],
                                        ]
                                    ],
                                    'column-3' => [
                                        'class' => Column::class,
                                        'settings' => [
                                            'm' => 5,
                                        ],
                                        'contents' => [
                                            'content-card' => [
                                                'class' => Card::class,
                                                'settings' => [
                                                    'title' => Yii::t('fafcms-filemanager', 'Web access'),
                                                    'icon' => 'web',
                                                ],
                                                'contents' => [
                                                    [
                                                        'class' => FormField::class,
                                                        'settings' => [
                                                            'field' => 'is_public',
                                                        ],
                                                    ],
                                                ]
                                            ],
                                        ]
                                    ],
                                ],
                            ],
                            'row-2' => [
                                'class' => Row::class,
                                'contents' => [
                                    'column-1' => [
                                        'class' => Column::class,
                                        'contents' => [
                                            'content-card' => [
                                                'class' => Card::class,
                                                'settings' => [
                                                    'title' => Yii::t('fafcms-filemanager', 'Files'),
                                                    'icon' => 'file-multiple-outline',
                                                ],
                                                'contents' => [
                                                    [
                                                        'class' => FormField::class,
                                                        'settings' => [
                                                            'field' => 'files',
                                                            'label' => ''
                                                        ],
                                                    ],
                                                ]
                                            ],
                                        ]
                                    ],
                                ],
                            ]
                        ]
                    ],
                ],
            ],
        ];
    }
    //endregion EditViewInterface implementation

    /**
     * @return bool
     */
    public function getIsNewRecord(): bool
    {
        return true;
    }

    /**
     * @return bool
     */
    public function getAddLoaderToSubmitButtons(): bool
    {
        return false;
    }

    /**
     * @return bool
     */
    public function getAllowAjaxForm(): bool
    {
        return false;
    }

    /**
     * @return array
     */
    public function getFormOptions(): array
    {
        return [
            'options' => [
                'enctype' => 'multipart/form-data',
            ],
        ];
    }

    /**
     * @return array|array[]
     */
    public function attributeOptions(): array
    {
        return [
            'status' => [
                'active'   => Yii::t('fafcms-core', 'Active'),
                'inactive' => Yii::t('fafcms-core', 'Inactive'),
            ],
        ];
    }

    public function loadDefaultValues($skipIfSet = true): self
    {
        $result = parent::loadDefaultValues($skipIfSet);

        try {
            $fileModule = Module::getLoadedModule();

            if ($this->filegroup_id === null) {
                $this->filegroup_id = Yii::$app->request->get('filegroup_id');
            }

            if ($this->filegroup_id === null) {
                if (!$skipIfSet || $this->create_new_folder === null) {
                    $this->create_new_folder = $fileModule->getPluginSettingValue('default_upload_folder_create_new');
                }

                if ((!$skipIfSet || $this->new_folder === null) && !empty($defaultUploadFolderNewPath = $fileModule->getPluginSettingValue('default_upload_folder_new_path'))) {
                    $this->new_folder = Yii::$app->fafcmsParser->parse(
                        Parser::TYPE_PAGE,
                        $defaultUploadFolderNewPath,
                        Parser::ROOT,
                        ['fileUploadForm' => $this]
                    );
                }
            }

            if ($this->is_public === null) {
                $this->is_public = $fileModule->getPluginSettingValue('default_upload_file_is_public');
            }

            if (!$skipIfSet || $this->filegroup_id === null) {
                $this->filegroup_id = $fileModule->getPluginSettingValue('default_upload_folder_filegroup_id');
            }
        } catch (ErrorException $e) {
        }

        return $result;
    }

    /**
     * @param string $sizeString
     *
     * @return int
     */
    public function parseIniSize(string $sizeString): int
    {
        $sizeUnit = strtolower(substr($sizeString, -1));

        $unitMap = [
            'm' => 1048576,
            'k' => 1024,
            'g' => 1073741824,
        ];

        if (isset($unitMap[$sizeUnit])) {
            return substr($sizeString, 0, -1) * $unitMap[$sizeUnit];
        }

        return (int)$sizeString;
    }

    /**
     * {@inheritdoc}
     */
    public function init(): void
    {
        $this->filetypes = Filetype::find()->asArray()->all();

        if ($this->maxFileSize === null) {
            $this->maxFileSize = $this->parseIniSize(ini_get('upload_max_filesize'));
        }

        if ($this->maxFiles === null) {
            $this->maxFiles = ini_get('max_file_uploads');
        }

        $extensions = [$this->extensions];

        foreach ($this->filetypes as $filetype) {
            $extensions[] = FileHelper::getExtensionsByMimeType($filetype['mime_type']);
        }

        $this->extensions = array_merge(...$extensions);
        $this->extensions = array_unique($this->extensions);
        sort($this->extensions);

        parent::init();
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        $result = parent::rules();
        /*var_dump($result);
        die();*/
        return [
            [['display_start', 'display_end'], 'safe'],
            [['files'], 'file', 'skipOnEmpty' => false, 'extensions' => $this->extensions, 'maxFiles' => $this->maxFiles, 'maxSize' => $this->maxFileSize],
            [['filegroup_id', 'is_public'], 'required'],
            [['status'], 'string', 'max' => 255],
            [['new_folder'], 'string'],
            [['filegroup_id'], 'integer'],
            [['is_public', 'create_new_folder'], 'boolean'],
            [
                ['filegroup_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Filegroup::class,
                'targetAttribute' => ['filegroup_id' => 'id'],
                'when'            => static function ($model) {
                    return (int)$model->filegroup_id !== 0;
                },
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return array_merge(
            File::instance()->attributeLabels(),
            [
                'files'        => Yii::t('fafcms-filemanager', 'Files'),
                'filegroup_id' => Yii::t('fafcms-filemanager', 'Destination folder'),
            ]
        );
    }

    /**
     * @param bool $runValidation
     * @param null $attributeNames
     *
     * @return bool
     * @throws ErrorException
     * @throws NotAcceptableHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function save($runValidation = true, $attributeNames = null): bool
    {
        $this->files = UploadedFile::getInstances($this, 'files');

        if ($this->validate()) {
            /*var_dump($this->getTags());
            die();*/
            $fileModule = Module::getLoadedModule();
            $filetypesByMime = ArrayHelper::index($this->filetypes, 'mime_type');

            foreach ($this->files as $uploadedFile) {
                $mimeType = FileHelper::getMimeTypeByExtension($uploadedFile->name);
                $filegroupId = (int)$this->filegroup_id;

                if ((bool)$this->create_new_folder) {
                    $newFolder = $this->new_folder;

                    if ($newFolder[0] !== '/' && $filegroupId !== 0) {
                        $filegroup = Filegroup::getPath($filegroupId, true);
                        $newFolder = $filegroup . '/' . $newFolder;
                    }

                    $filegroup = $fileModule->getGetFilegroupByPath($newFolder);
                    $filegroupId = $filegroup['id'];
                }

                if (isset($filetypesByMime[$mimeType])) {
                    $file = new File();
                    $filetypeId = $filetypesByMime[$mimeType]['id'];

                    $fileNumber = File::find()->max('number');

                    if (empty($fileNumber)) {
                        $fileNumber = (int)$fileModule->getPluginSettingValue('file_number_start_value');
                    } else {
                        $fileNumber += (int)$fileModule->getPluginSettingValue('file_number_step_size');
                    }

                    $filename = Yii::$app->fafcmsParser->parse(
                        Parser::TYPE_PAGE,
                        $fileModule->getPluginSettingValue('default_upload_filename_format'),
                        Parser::ROOT,
                        [
                            'fileName'       => $uploadedFile->baseName,
                            'fileNumber'     => $fileNumber,
                            'filegroup'      => static function () use ($filegroupId) {
                                return Filegroup::findOne($filegroupId);
                            },
                            'filetype'       => static function () use ($filetypeId) {
                                return Filetype::findOne($filetypeId);
                            },
                            'fileUploadForm' => $this,
                            'uploadedFile'   => $uploadedFile,
                        ]
                    );

                    $file->filename = $filename;
                    $file->number = $fileNumber;
                    $file->filetype_id = $filetypeId;
                    $file->filegroup_id = $filegroupId;
                    $file->size = $uploadedFile->size;
                    $file->is_public = $this->is_public;
                    $file->status = $this->status;
                    $file->display_start = $this->display_start;
                    $file->display_end = $this->display_end;

                    $fileTarget = File::getFileTarget($file);

                    try {
                        FileHelper::createDirectory($fileTarget);
                    } catch (Exception $e) {
                        throw new NotAcceptableHttpException('Destination folder \'' . $fileTarget . '\' cannot be created..');
                    }

                    if ($file->validate() && $file->save()) {
                        $uploadedFile->saveAs(File::getFilePath($file));
                    } else {
                        throw new NotAcceptableHttpException(print_r($file->getErrors(), true));
                    }
                } else {
                    throw new NotAcceptableHttpException('Wrong mimetype.' . $mimeType);
                }
            }

            return true;
        }

        //throw new NotAcceptableHttpException(print_r($this->getErrors(),true));

        return false;
    }
}
