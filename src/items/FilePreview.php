<?php

namespace fafcms\filemanager\items;

use fafcms\fafcms\inputs\Checkbox;
use fafcms\fafcms\inputs\TextInput;
use fafcms\helpers\classes\ContentItemSetting;
use Yii;
use fafcms\helpers\abstractions\ContentItem;
use fafcms\fafcms\components\FafcmsComponent;
use yii\base\InvalidArgumentException;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use fafcms\filemanager\models\File;
use fafcms\filemanager\models\Filegroup;

/**
 * Class FilePreview
 *
 * @package fafcms\filemanager\items
 */
class FilePreview extends ContentItem
{
    public int $type = self::TYPE_ITEM;

    public function label(): string
    {
        return Yii::t('fafcms-core', 'File preview');
    }

    public function description(): string
    {
        return Yii::t('fafcms-core', 'Shows the preview of an file.');
    }

    public function itemSettings(): ?array
    {
        return [
            new ContentItemSetting($this, [
                'label' => Yii::t('fafcms-core', 'File id'),
                'name' => 'fileId',
                'valueType' => FafcmsComponent::VALUE_TYPE_INT,
                'inputType' => NumberInput::class
            ]),
            new ContentItemSetting($this, [
                'label' => Yii::t('fafcms-core', 'Field name'),
                'name' => 'field',
                'valueType' => FafcmsComponent::VALUE_TYPE_VARCHAR,
                'inputType' => TextInput::class
            ]),
        ];
    }

    public function run(): string
    {
        $fileId = $this->getSetting('fileId');

        if ($fileId === null) {
            $fieldName = $this->getSetting('field');
            $model = Yii::$app->view->params['model'];

            if (!$model->hasAttribute($fieldName)) {
                throw new InvalidArgumentException('Unknown form field "'.$fieldName.'" in model "'.get_class($model).'".');
            }

            $fileId = $model->getAttribute($fieldName);
        }

        $file = File::find()->where(['id' => $fileId])->one();

        if ($file === null) {
            return '';
        }

        if ($file['filetype']['mime_type'] === 'application/pdf') {
            $url = Url::to([Filegroup::instance()->getEditData()['url'] . '/view-pdf', 'file' => Url::to([Filegroup::instance()->getEditData()['url'] . '/get-file', 'id' => $file->id], true), '#' => 'zoom=page-fit']);
            return Html::tag('iframe', '', ['src' => $url, 'class' => 'filemanager-preview', 'frameborder' => '0', 'allowfullscreen' => true]);
        }

        if ($file['filetype']['mime_type'] === 'image/svg+xml') {
            return Html::tag('div', Html::img(File::getUrl($file), ['class' => 'full-width']), ['class' => 'fafcms-filemanager-image-wrapper']);
        }

        if ($file['filetype']['mediatype'] === 'video') {
            return File::getVideo($file, 'Filemanager preview');
        }

        return File::getPicture($file, 'Filemanager preview', [
            [500, 500, 'bestfit:true;'],
            [75, 75, 'bestfit:true;']
        ], true, null);
    }
}
