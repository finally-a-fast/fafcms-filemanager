<?php
namespace fafcms\filemanager\assets;

use yii\web\AssetBundle;
use fafcms\fafcms\components\ViewComponent;

class PdfJsAsset extends AssetBundle
{
    public $sourcePath = '@vendor/clean-composer-packages/pdf-js';

    public $js = [
        'build/pdf.js',
        'web/viewer.js'
    ];

    public $jsOptions = [
        'position' => ViewComponent::POS_HEAD
    ];

    public $css = [
        'web/viewer.css'
    ];
}
