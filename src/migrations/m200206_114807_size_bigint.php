<?php

namespace fafcms\filemanager\migrations;

use fafcms\filemanager\models\Archive;
use fafcms\filemanager\models\File;
use yii\db\Migration;

/**
 * Class m200206_114807_size_bigint
 * @package fafcms\filemanager\migrations
 */
class m200206_114807_size_bigint extends Migration
{
    public function safeUp()
    {
        $this->alterColumn(File::tableName(), 'size', $this->bigInteger(20)->unsigned()->notNull());
        $this->alterColumn(Archive::tableName(), 'size', $this->bigInteger(20)->unsigned()->notNull());
    }

    public function safeDown()
    {
        $this->alterColumn(File::tableName(), 'size', $this->integer(10)->unsigned()->notNull());
        $this->alterColumn(Archive::tableName(), 'size', $this->integer(10)->unsigned()->notNull());
    }
}
