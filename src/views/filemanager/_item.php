<?php

use yii\helpers\Html;

/**
 * @var $badges array
 * @var $name string
 * @var $editLink array
 * @var $downloadLink array|null
 * @var $deleteLink array
 * @var $removeLink array|null
 */

$info = '';

foreach ($badges as $badge) {
    $info .= ($info !== '' ? ' ' : '') . Html::tag('i', '', ['class' => 'mdi mdi-' . $badge['icon']]) . ' ' . $badge['count'];
}
?>
<div class="text-scroller"><span class="ui small text"><strong><?= $name ?></strong></span></div>
<span class="ui small text"><?= $info ?></span>
<br><div class="ui icon buttons mini">
    <?php
        if (isset($removeLink)) {
            Html::addCssClass($removeLink, 'ui button');
            echo Html::button('<i class="icon mdi mdi-close-circle-outline"></i>', $removeLink);
        }
        else {
            echo Html::a('<i class="icon mdi mdi-pencil-outline"></i>', $editLink, ['class' => 'ui button']);
        }
    ?>
    <div class="ui icon top right pointing dropdown button" tabindex="0">
        <i class="icon mdi mdi-dots-horizontal"></i>
        <div class="menu">
            <?php if (isset($removeLink)): ?>
                <?= Html::a('<i class="icon mdi mdi-pencil-outline"></i>' . Yii::t('fafcms-core', 'Edit'), $editLink, ['class' => 'item']) ?>
            <?php endif ?>
            <?= Html::a('<i class="icon mdi mdi-content-copy"></i>' . Yii::t('fafcms-core', 'Copy name'), null, [
                'class' => 'item copy-text',
                'data-copy-text' => $name,
            ]) . ($downloadLink === null ? '' : Html::a('<i class="icon mdi mdi-download"></i>' . Yii::t('fafcms-core', 'Download'), $downloadLink, [
                'class' => 'item',
                'target' => '_blank',
            ])) . Html::a('<i class="icon mdi mdi-delete-outline"></i>' . Yii::t('yii', 'Delete'), $deleteLink, [
                'class' => 'item',
                'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                'data-method' => 'post'
            ]) ?>
        </div>
    </div>
</div>
