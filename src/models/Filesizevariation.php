<?php

namespace fafcms\filemanager\models;

use fafcms\filemanager\abstracts\models\BaseFilesizevariation;

/**
 * This is the model class for table "{{%filesizevariation}}".
 *
 * @package fafcms\filemanager\models
 */
class Filesizevariation extends BaseFilesizevariation
{

}
