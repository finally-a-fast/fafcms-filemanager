[![Finally a fast CMS](https://www.finally-a-fast.com/logos/logo-cms-readme.jpg)](https://www.finally-a-fast.com/) | Docs | Guide | Module file manager
================================================

[[_TOC_]]

Placeholders
------------

### media

### embed-image
Returns img tag with file path (instead of url) to embet images inside of PDFs (Only works with renderer of typ `Parser::TYPE_PDF`). 
