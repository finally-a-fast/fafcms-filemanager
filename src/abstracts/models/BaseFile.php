<?php

namespace fafcms\filemanager\abstracts\models;

use app\models\{
    Cart,
    FileArticle,
};
use fafcms\documentmanager\{
    models\Document,
    models\Documentversion,
};
use fafcms\fafcms\{
    inputs\DateTimePicker,
    inputs\DropDownList,
    inputs\NumberInput,
    inputs\ExtendedDropDownList,
    inputs\SwitchCheckbox,
    inputs\Textarea,
    inputs\TextInput,
    items\ActionColumn,
    items\Card,
    items\Column,
    items\DataColumn,
    items\FormField,
    items\Row,
    items\Tab,
    models\User,
};
use fafcms\filemanager\{
    inputs\FileSelect,
    models\Filegroup,
    models\Filesizevariation,
    models\Filetype,
};
use fafcms\helpers\{
    ActiveRecord,
    interfaces\EditViewInterface,
    interfaces\FieldConfigInterface,
    interfaces\IndexViewInterface,
    traits\AttributeOptionTrait,
    traits\BeautifulModelTrait,
    traits\OptionTrait,
};
use fafcms\twitterapi\models\Attachment;
use fafcms\youtubeapi\models\Thumbnail;
use Yii;
use yii\db\ActiveQuery;
use yii\validators\DateValidator;

/**
 * This is the abstract model class for table "{{%file}}".
 *
 * @package fafcms\filemanager\abstracts\models
 *
 * @property-read array $fieldConfig
 *
 * @property int $id
 * @property string $status
 * @property string|null $display_start
 * @property string|null $display_end
 * @property int $filegroup_id
 * @property int $filetype_id
 * @property string $filename
 * @property int $number
 * @property string|null $alt
 * @property int $size
 * @property string|null $meta
 * @property int $allow_download
 * @property int $is_public
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $activated_by
 * @property int|null $deactivated_by
 * @property int|null $deleted_by
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $activated_at
 * @property string|null $deactivated_at
 * @property string|null $deleted_at
 *
 * @property User $activatedBy
 * @property Document[] $activeFileDocuments
 * @property User $createdBy
 * @property User $deactivatedBy
 * @property User $deletedBy
 * @property Attachment[] $fileAttachments
 * @property Cart[] $fileCarts
 * @property Documentversion[] $fileDocumentversions
 * @property FileArticle[] $fileFileArticles
 * @property Filesizevariation[] $fileFilesizevariations
 * @property Thumbnail[] $fileThumbnails
 * @property Filegroup $filegroup
 * @property Filetype $filetype
 * @property User $updatedBy
 */
abstract class BaseFile extends ActiveRecord implements FieldConfigInterface, IndexViewInterface, EditViewInterface
{
    use BeautifulModelTrait;
    use OptionTrait;
    use AttributeOptionTrait;

    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataUrl($model): string
    {
        return 'file';
    }

    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return  'file';
    }

    /**
     * @inheritDoc
     */
    public static function editDataPlural($model): string
    {
        return Yii::t('fafcms-file', 'Files');
    }

    /**
     * @inheritDoc
     */
    public static function editDataSingular($model): string
    {
        return Yii::t('fafcms-file', 'File');
    }

    /**
     * @inheritDoc
     */
    public static function extendedLabel($model, bool $html = true, array $params = []): string
    {
        return trim(($model['id'] ?? ''));
    }
    //endregion BeautifulModelTrait implementation

    //region OptionTrait implementation
    /**
     * @inheritDoc
     */
    public static function getOptions(bool $empty = true, array $select = null, array $sort = null, array $where = null, array $joinWith = null, string $emptyLabel = null): array
    {
        $options = Yii::$app->dataCache->map(
            static::class,
            array_merge([
                static::tableName() . '.id',
                static::tableName() . '.id'
            ], $select ?? []),
            'id',
            static function ($item) {
                return static::extendedLabel($item);
            },
            null,
            $sort ?? [static::tableName() . '.id' => SORT_ASC],
            $where,
            $joinWith
        );

        return OptionTrait::getOptions($empty, $select, $sort, $where, $joinWith, $emptyLabel) + $options;
    }
    //endregion OptionTrait implementation

    //region AttributeOptionTrait implementation
    /**
     * @inheritDoc
     */
    public function attributeOptions(): array
    {
        return [
            'status' => [
                static::STATUS_ACTIVE => Yii::t('fafcms-core', 'Active'),
                static::STATUS_INACTIVE => Yii::t('fafcms-core', 'Inactive'),
            ],
            'filegroup_id' => static function(...$params) {
                return Filegroup::getOptions(...$params);
            },
            'filetype_id' => static function(...$params) {
                return Filetype::getOptions(...$params);
            },
            'created_by' => static function(...$params) {
                return User::getOptions(...$params);
            },
            'updated_by' => static function(...$params) {
                return User::getOptions(...$params);
            },
            'activated_by' => static function(...$params) {
                return User::getOptions(...$params);
            },
            'deactivated_by' => static function(...$params) {
                return User::getOptions(...$params);
            },
            'deleted_by' => static function(...$params) {
                return User::getOptions(...$params);
            },
        ];
    }
    //endregion AttributeOptionTrait implementation

    //region FieldConfigInterface implementation
    public function getFieldConfig(): array
    {
        return [
            'id' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'status' => [
                'type' => DropDownList::class,
                'items' => $this->attributeOptions()['status'],
            ],
            'display_start' => [
                'type' => DateTimePicker::class,
            ],
            'display_end' => [
                'type' => DateTimePicker::class,
            ],
            'filegroup_id' => [
                'type' => FileSelect::class,
                'external' => false,
            ],
            'filetype_id' => [
                'type' => FileSelect::class,
                'external' => false,
            ],
            'filename' => [
                'type' => FileSelect::class,
            ],
            'number' => [
                'type' => NumberInput::class,
            ],
            'alt' => [
                'type' => TextInput::class,
            ],
            'size' => [
                'type' => NumberInput::class,
            ],
            'meta' => [
                'type' => Textarea::class,
            ],
            'allow_download' => [
                'type' => SwitchCheckbox::class,
                'offLabel' => [
                    'fafcms-core',
                    'No',
                ],
                'onLabel' => [
                    'fafcms-core',
                    'Yes',
                ],
            ],
            'is_public' => [
                'type' => SwitchCheckbox::class,
                'offLabel' => [
                    'fafcms-core',
                    'No',
                ],
                'onLabel' => [
                    'fafcms-core',
                    'Yes',
                ],
            ],
            'created_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->attributeOptions()['created_by'],
                'relationClassName' => User::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'updated_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->attributeOptions()['updated_by'],
                'relationClassName' => User::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'activated_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->attributeOptions()['activated_by'],
                'relationClassName' => User::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deactivated_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->attributeOptions()['deactivated_by'],
                'relationClassName' => User::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deleted_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->attributeOptions()['deleted_by'],
                'relationClassName' => User::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'created_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'updated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'activated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deactivated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deleted_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
        ];
    }
    //endregion FieldConfigInterface implementation

    //region IndexViewInterface implementation
    public static function indexView(): array
    {
        return [
            'default' => [
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'id',
                        'sort' => 1,
                        'link' => true,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'status',
                        'sort' => 2,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'display_start',
                        'sort' => 3,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'display_end',
                        'sort' => 4,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'filegroup_id',
                        'sort' => 5,
                        'link' => true,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'filetype_id',
                        'sort' => 6,
                        'link' => true,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'filename',
                        'sort' => 7,
                        'link' => true,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'number',
                        'sort' => 8,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'alt',
                        'sort' => 9,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'size',
                        'sort' => 10,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'meta',
                        'sort' => 11,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'allow_download',
                        'sort' => 12,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'is_public',
                        'sort' => 13,
                    ],
                ],
                [
                    'class' => ActionColumn::class,
                ],
            ]
        ];
    }
    //endregion IndexViewInterface implementation

    //region EditViewInterface implementation
    public static function editView(): array
    {
        return [
            'default' => [
                'tab-1' => [
                    'class' => Tab::class,
                    'settings' => [
                        'label' => [
                            'fafcms-core',
                            'Master data',
                        ],
                    ],
                    'contents' => [
                        'row-1' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 8,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-core',
                                                    'Master data',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-status' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'status',
                                                    ],
                                                ],
                                                'field-display_start' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'display_start',
                                                    ],
                                                ],
                                                'field-display_end' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'display_end',
                                                    ],
                                                ],
                                                'field-filegroup_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'filegroup_id',
                                                    ],
                                                ],
                                                'field-filetype_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'filetype_id',
                                                    ],
                                                ],
                                                'field-filename' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'filename',
                                                    ],
                                                ],
                                                'field-number' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'number',
                                                    ],
                                                ],
                                                'field-alt' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'alt',
                                                    ],
                                                ],
                                                'field-size' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'size',
                                                    ],
                                                ],
                                                'field-meta' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'meta',
                                                    ],
                                                ],
                                                'field-allow_download' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'allow_download',
                                                    ],
                                                ],
                                                'field-is_public' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'is_public',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
    //endregion EditViewInterface implementation

    /**
     * {@inheritdoc}
     */
    public static function prefixableTableName(): string
    {
        return '{{%file}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            'date-display_start' => ['display_start', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'display_start', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-display_end' => ['display_end', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'display_end', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-created_at' => ['created_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'created_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-updated_at' => ['updated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'updated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-activated_at' => ['activated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'activated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-deactivated_at' => ['deactivated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'deactivated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-deleted_at' => ['deleted_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'deleted_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'required-filegroup_id' => ['filegroup_id', 'required'],
            'required-filetype_id' => ['filetype_id', 'required'],
            'required-filename' => ['filename', 'required'],
            'required-size' => ['size', 'required'],
            'integer-filegroup_id' => ['filegroup_id', 'integer'],
            'integer-filetype_id' => ['filetype_id', 'integer'],
            'integer-number' => ['number', 'integer'],
            'integer-size' => ['size', 'integer'],
            'integer-created_by' => ['created_by', 'integer'],
            'integer-updated_by' => ['updated_by', 'integer'],
            'integer-activated_by' => ['activated_by', 'integer'],
            'integer-deactivated_by' => ['deactivated_by', 'integer'],
            'integer-deleted_by' => ['deleted_by', 'integer'],
            'string-meta' => ['meta', 'string'],
            'boolean-allow_download' => ['allow_download', 'boolean'],
            'boolean-is_public' => ['is_public', 'boolean'],
            'string-status' => ['status', 'string', 'max' => 255],
            'string-filename' => ['filename', 'string', 'max' => 255],
            'string-alt' => ['alt', 'string', 'max' => 255],
            'exist-activated_by' => [['activated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['activated_by' => 'id']],
            'exist-created_by' => [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['created_by' => 'id']],
            'exist-deactivated_by' => [['deactivated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['deactivated_by' => 'id']],
            'exist-deleted_by' => [['deleted_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['deleted_by' => 'id']],
            'exist-filegroup_id' => [['filegroup_id'], 'exist', 'skipOnError' => true, 'targetClass' => Filegroup::class, 'targetAttribute' => ['filegroup_id' => 'id']],
            'exist-filetype_id' => [['filetype_id'], 'exist', 'skipOnError' => true, 'targetClass' => Filetype::class, 'targetAttribute' => ['filetype_id' => 'id']],
            'exist-updated_by' => [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['updated_by' => 'id']],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return array_merge(parent::attributeLabels(), [
            'id' => Yii::t('fafcms-file', 'ID'),
            'status' => Yii::t('fafcms-file', 'Status'),
            'display_start' => Yii::t('fafcms-file', 'Display Start'),
            'display_end' => Yii::t('fafcms-file', 'Display End'),
            'filegroup_id' => Yii::t('fafcms-file', 'Filegroup ID'),
            'filetype_id' => Yii::t('fafcms-file', 'Filetype ID'),
            'filename' => Yii::t('fafcms-file', 'Filename'),
            'number' => Yii::t('fafcms-file', 'Number'),
            'alt' => Yii::t('fafcms-file', 'Alt'),
            'size' => Yii::t('fafcms-file', 'Size'),
            'meta' => Yii::t('fafcms-file', 'Meta'),
            'allow_download' => Yii::t('fafcms-file', 'Allow Download'),
            'is_public' => Yii::t('fafcms-file', 'Is Public'),
            'created_by' => Yii::t('fafcms-file', 'Created By'),
            'updated_by' => Yii::t('fafcms-file', 'Updated By'),
            'activated_by' => Yii::t('fafcms-file', 'Activated By'),
            'deactivated_by' => Yii::t('fafcms-file', 'Deactivated By'),
            'deleted_by' => Yii::t('fafcms-file', 'Deleted By'),
            'created_at' => Yii::t('fafcms-file', 'Created At'),
            'updated_at' => Yii::t('fafcms-file', 'Updated At'),
            'activated_at' => Yii::t('fafcms-file', 'Activated At'),
            'deactivated_at' => Yii::t('fafcms-file', 'Deactivated At'),
            'deleted_at' => Yii::t('fafcms-file', 'Deleted At'),
        ]);
    }

    /**
     * Gets query for [[ActivatedBy]].
     *
     * @return ActiveQuery
     */
    public function getActivatedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'activated_by',
        ]);
    }

    /**
     * Gets query for [[ActiveFileDocuments]].
     *
     * @return ActiveQuery
     */
    public function getActiveFileDocuments(): ActiveQuery
    {
        return $this->hasMany(Document::class, [
            'active_file_id' => 'id',
        ]);
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return ActiveQuery
     */
    public function getCreatedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'created_by',
        ]);
    }

    /**
     * Gets query for [[DeactivatedBy]].
     *
     * @return ActiveQuery
     */
    public function getDeactivatedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'deactivated_by',
        ]);
    }

    /**
     * Gets query for [[DeletedBy]].
     *
     * @return ActiveQuery
     */
    public function getDeletedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'deleted_by',
        ]);
    }

    /**
     * Gets query for [[FileAttachments]].
     *
     * @return ActiveQuery
     */
    public function getFileAttachments(): ActiveQuery
    {
        return $this->hasMany(Attachment::class, [
            'file_id' => 'id',
        ]);
    }

    /**
     * Gets query for [[FileCarts]].
     *
     * @return ActiveQuery
     */
    public function getFileCarts(): ActiveQuery
    {
        return $this->hasMany(Cart::class, [
            'file_id' => 'id',
        ]);
    }

    /**
     * Gets query for [[FileDocumentversions]].
     *
     * @return ActiveQuery
     */
    public function getFileDocumentversions(): ActiveQuery
    {
        return $this->hasMany(Documentversion::class, [
            'file_id' => 'id',
        ]);
    }

    /**
     * Gets query for [[FileFileArticles]].
     *
     * @return ActiveQuery
     */
    public function getFileFileArticles(): ActiveQuery
    {
        return $this->hasMany(FileArticle::class, [
            'file_id' => 'id',
        ]);
    }

    /**
     * Gets query for [[FileFilesizevariations]].
     *
     * @return ActiveQuery
     */
    public function getFileFilesizevariations(): ActiveQuery
    {
        return $this->hasMany(Filesizevariation::class, [
            'file_id' => 'id',
        ]);
    }

    /**
     * Gets query for [[FileThumbnails]].
     *
     * @return ActiveQuery
     */
    public function getFileThumbnails(): ActiveQuery
    {
        return $this->hasMany(Thumbnail::class, [
            'file_id' => 'id',
        ]);
    }

    /**
     * Gets query for [[Filegroup]].
     *
     * @return ActiveQuery
     */
    public function getFilegroup(): ActiveQuery
    {
        return $this->hasOne(Filegroup::class, [
            'id' => 'filegroup_id',
        ]);
    }

    /**
     * Gets query for [[Filetype]].
     *
     * @return ActiveQuery
     */
    public function getFiletype(): ActiveQuery
    {
        return $this->hasOne(Filetype::class, [
            'id' => 'filetype_id',
        ]);
    }

    /**
     * Gets query for [[UpdatedBy]].
     *
     * @return ActiveQuery
     */
    public function getUpdatedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'updated_by',
        ]);
    }
}
