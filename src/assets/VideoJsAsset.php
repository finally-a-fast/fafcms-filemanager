<?php
namespace fafcms\filemanager\assets;

use yii\web\AssetBundle;

class VideoJsAsset extends AssetBundle
{
    public $sourcePath = '@npm/video.js/dist';

    public $css = [
        'video-js.min.css',
    ];

    public $js = [
        'video.min.js',
        'lang/de.js',
    ];
}
