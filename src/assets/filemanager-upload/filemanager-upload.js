function FilemanagerUpload() {
  let cmsModule = this
  let fafAjaxModal = window.faf.FafcmsAjaxModal

  let isOpening = false
  let uploadedFile = false

  let finishFileUpload = function() {
    if (uploadedFile) {
      window.location.reload()
    }
  }

  fafAjaxModal.modalEvents['fafcms-file-upload-modal'] = {
    closeEnd: [
      finishFileUpload,
      function () {
        document.getElementById('fafcms-file-upload-modal').remove()
        isOpening = false
      }
    ]
  }

  window.faf.events['windowDragenter'].push(function(e) {
    let uploadModal = document.getElementById('fafcms-file-upload-modal')

    if (
      !isOpening &&
      (uploadModal === null || !uploadModal.classList.contains('visible')) &&
      typeof e.dataTransfer !== 'undefined' &&
      typeof e.dataTransfer.types !== 'undefined'
    ) {
      for (let i = 0; i < e.dataTransfer.types.length; i++) {
        if (e.dataTransfer.types[i] === 'Files') {
          isOpening = true

          fafAjaxModal.initModal('fafcms-file-upload-modal')

          fafAjaxModal.openModal({
            url: '/' + window.faf.config.backend + '/fafcms-filemanager/file/upload',
            footerSelector: '.card-action',
            footerRemoveFromContent: 'true',
            modalId: 'fafcms-file-upload-modal',
            modalEvents: {
              openStart: fafAjaxModal.modalEvents['fafcms-file-upload-modal'].openStart,
              openEnd: fafAjaxModal.modalEvents['fafcms-file-upload-modal'].openEnd,
              closeStart: fafAjaxModal.modalEvents['fafcms-file-upload-modal'].closeStart,
              closeEnd: fafAjaxModal.modalEvents['fafcms-file-upload-modal'].closeEnd,
              contentLoaded: fafAjaxModal.modalEvents['fafcms-file-upload-modal'].contentLoaded
            },
          })

          break
        }
      }
    }
  })

  let initFileUploads = function(target) {
    let elements = window.faf.app.findChildren(target, '.fafcms-file-upload:not(.fafcms-file-upload-initialized)')

    for (let i = 0; i < elements.length; i++) {
      let dataset = elements[i].dataset
      let $uploadForm = $('#' + dataset.formId)
      let uploadForm = document.getElementById(dataset.formId)

      let getSubmitButtons = function(form) {
        let submitButtons = []
        let insideSubmitButtons = form.querySelectorAll('button[type="submit"], input[type="submit"]')
        let outsideSubmitButtons = document.querySelectorAll('button[type="submit"][form="' + form.getAttribute('id') + '"')

        for (let x = 0; x < insideSubmitButtons.length; x++) {
          submitButtons.push(insideSubmitButtons[x])
        }

        for (let x = 0; x < outsideSubmitButtons.length; x++) {
          submitButtons.push(outsideSubmitButtons[x])
        }

        return submitButtons
      }

      $uploadForm.dropzone({
          paramName: dataset.inputName,
          acceptedFiles: dataset.acceptedFileExtensions,
          maxFilesize: dataset.maxFileSize,
          maxFiles: dataset.maxFiles,
          parallelUploads: 10,
          autoQueue: false,
          createImageThumbnails: false,
          dictFileTooBig: window.faf.app.t('fafcms-filemanager', 'File is too big ({{filesize}}MiB). Max filesize: {{maxFilesize}}MiB.'),
          dictMaxFilesExceeded: window.faf.app.t('fafcms-filemanager', 'You can not upload any more files.'),
          dictInvalidFileType: window.faf.app.t('fafcms-filemanager', 'You can\'t upload files of this type.'),
          init: function () {
            let mainDropzone = this

              $uploadForm.on('submit', function (e) {
              e.preventDefault()
            })

              $('body').on('click', '#' + $uploadForm.attr('id') + ' button[type="submit"], #' + $uploadForm.attr('id') + ' input[type="submit"], button[type="submit"][form="' + $uploadForm.attr('id') + '"]', function () {
              mainDropzone.enqueueFiles(mainDropzone.getFilesWithStatus(Dropzone.ADDED))
            })

            mainDropzone.on('error', function (file, errorMessage, a) {
              window.faf.FafcmsToast.toast({message: errorMessage, type: 'error'})
            })

            mainDropzone.on('complete', function (file) {
              let errorMessage = ''

              if (mainDropzone.getFilesWithStatus(Dropzone.PROCESSING).length === 0 && mainDropzone.getFilesWithStatus(Dropzone.UPLOADING).length === 0) {
                window.faf.FafcmsForm.removeLoaderFromButtons(getSubmitButtons(uploadForm))
              }

              mainDropzone.removeFile(file)

              if (file.status === 'success') {
                response = window.faf.FafcmsForm.handleAjaxResponse(file.xhr.response)

                if (typeof response.type !== 'undefined' && response.type === 'success') {
                  return
                }

                errorMessage = response.message
              } else {
                if (typeof file.xhr === 'undefined') {
                  return
                }

                errorMessage = window.faf.app.t('fafcms-filemanager', '{filename} could not be uploaded.', {filename: file.name}) + '<br>' + file.xhr.response
                window.faf.FafcmsToast.toast({message: errorMessage, type: 'error'})
              }

              mainDropzone.addFile(file)

              $(file.previewElement).find('.filemanager-list-item-error').html('<p>' + errorMessage + '</p>')
              $(file.previewElement).find('.filemanager-list-item-error').removeClass('hide')
            })

            mainDropzone.on('sending', function(file) {
              $(file.previewElement).find('.progress').removeClass('hide')
              $(file.previewElement).find('.filemanager-list-item-error').addClass('hide')
              $(file.previewElement).find('.filemanager-list-item-upload').hide()

              uploadedFile = true
              window.faf.FafcmsForm.addLoaderToButtons(getSubmitButtons(uploadForm))
            })

            mainDropzone.on('addedfile', function (file) {
              $(file.previewElement).find('.filemanager-list-item-upload').click(function() {
                mainDropzone.enqueueFile(file)
              })
            })
          },
          previewTemplate: $uploadForm.find('.filemanager-upload-previews-template').html(),
          previewsContainer: dataset.uploadContainer + ' .filemanager-upload-previews'
      })

      elements[i].classList.add('fafcms-file-upload-initialized')
    }
  }


  cmsModule.init = function () {
    initFileUploads(document.getElementsByTagName('html'))

    Dropzone.prototype.filesize = function (size) {
      let cutoff
      let selectedSize = 0
      let selectedUnit = 'b'
      let units = ['tb', 'gb', 'mb', 'kb', 'b']
      let len = units.length
      let j = 0

      if (size > 0) {
        for (let i = 0; j < len; i = ++j) {
          let unit = units[i]

          cutoff = Math.pow(this.options.filesizeBase, 4 - i) / 10

          if (size >= cutoff) {
            selectedSize = size / Math.pow(this.options.filesizeBase, 4 - i)
            selectedUnit = unit
            break
          }
        }

        selectedSize = Math.round(10 * selectedSize) / 10
      }

      return selectedSize.toLocaleString() + ' ' + this.options.dictFileSizeUnits[selectedUnit]
    }
  }

  window.faf.events['initPlugins'].push(function (target) {
    initFileUploads(target)
  })
}
