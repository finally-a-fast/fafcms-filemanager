<?php
/**
 * @author    Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 Finally a fast
 * @license   https://www.finally-a-fast.com/packages/fafcms-module-filemanager/license MIT
 * @link      https://www.finally-a-fast.com/packages/fafcms-module-filemanager
 * @see       https://www.finally-a-fast.com/packages/fafcms-module-filemanager/docs Documentation of fafcms-module-filemanager
 * @since     File available since Release 1.0.0
 */

namespace fafcms\filemanager\controllers;

use fafcms\filemanager\models\Filegroup;
use fafcms\helpers\DefaultController;

/**
 * Class FilegroupController
 *
 * @package fafcms\filemanager\controllers
 */
class FilegroupController extends DefaultController
{
    public static $modelClass = Filegroup::class;
}
