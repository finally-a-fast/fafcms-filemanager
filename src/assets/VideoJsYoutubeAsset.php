<?php
namespace fafcms\filemanager\assets;

use yii\web\AssetBundle;

class VideoJsYoutubeAsset extends AssetBundle
{
    public $sourcePath = '@npm/videojs-youtube/dist';

    public $js = [
        'Youtube.min.js',
    ];

    public $depends = [
        'fafcms\filemanager\assets\VideoJsAsset',
    ];
}
