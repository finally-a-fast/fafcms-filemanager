<?php
namespace fafcms\filemanager\assets;

use yii\web\AssetBundle;

class MediaAsset extends AssetBundle
{
    public $sourcePath = __DIR__.'/media';

    public $css = [
        'css/media'.(YII_ENV_DEV?'':'.min').'.css',
    ];

    public $cssOptions = [
        'fafcmsAsync' => true,
    ];
}
