<?php

namespace fafcms\filemanager\models;

use fafcms\filemanager\abstracts\models\BaseFilegroup;
use fafcms\filemanager\Bootstrap;
use fafcms\helpers\traits\BeautifulModelTrait;
use fafcms\helpers\traits\ModelTrait;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Inflector;

/**
 * This is the model class for table "{{%filegroup}}".
 *
 * @package fafcms\filemanager\models
 */
class Filegroup extends BaseFilegroup
{
    use BeautifulModelTrait;

    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataUrl($model): string
    {
        return '/' . Bootstrap::$id . '/filegroup';
    }

    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return 'folder-outline';
    }

    /**
     * @inheritDoc
     */
    public static function editDataPlural($model): string
    {
        return Yii::t('fafcms-filemanager', 'Folder');
    }

    /**
     * @inheritDoc
     */
    public static function editDataSingular($model): string
    {
        return Yii::t('fafcms-filemanager', 'Folder');
    }
    //endregion BeautifulModelTrait implementation

    /**
     * @inheritDoc
     */
    public function loadDefaultValues($skipIfSet = true): self
    {
        $result = parent::loadDefaultValues($skipIfSet);

        if (!$skipIfSet || $result->parent_filegroup_id === null) {
            $result->parent_filegroup_id = (int)Yii::$app->request->get('parent_filegroup_id');
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            'exist-parent_filegroup_id' => [['parent_filegroup_id'], 'exist', 'skipOnError' => true, 'targetClass' => static::class, 'targetAttribute' => ['parent_filegroup_id' => 'id'], 'when' => static function ($model) {
                return (int)$model->parent_filegroup_id !== 0;
            }],
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumenttemplates()
    {
        return $this->hasMany(Documenttemplate::class, ['filegroup_id' => 'id'])->inverseOf('filegroup');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFiles()
    {
        return $this->hasMany(File::class, ['filegroup_id' => 'id'])->inverseOf('filegroup');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFilegroups()
    {
        return $this->hasMany(Filegroup::class, ['parent_filegroup_id' => 'id']);
    }

    public function getSelect($empty = false, $current = null)
    {
        $items = ($empty?[0 => Yii::t('fafcms-filemanager', 'Root')]:[]);
        $options = [];

        $this->getSelectOptions($items, $options, $current, null, 1);

        return [
            'items' => $items,
            'options' => [
                'options' => $options,
            ],
        ];
    }

    private $_filegroupOptions;

    private function filegroupOptionCache()
    {
        if ($this->_filegroupOptions === null) {
            $this->_filegroupOptions = ArrayHelper::index(Filegroup::find()->select('id, name, parent_filegroup_id')->orderBy('parent_filegroup_id, name')->asArray()->all(), null, 'parent_filegroup_id');
        }

        return $this->_filegroupOptions;
    }

    private function getSelectOptions( &$items, &$options, $current, $parentFilegroupId, $level, $parentDisabled = false)
    {
        foreach ($this->filegroupOptionCache()[$parentFilegroupId]??[] as $option) {
            $levelIcons = '';

            for ($i = 0; $i < $level; $i++) {
                $levelIcons .= '<i class="mdi mdi-subdirectory-arrow-right tiny"></i>';
            }

            $items[$option['id']] = $levelIcons.$option['name'];
            $disabled = false;

            if ($parentDisabled || $current !== null && $option['id'] == $current) {
                $disabled = true;
            }

            $options[$option['id']] = ['disabled' => $disabled];
            $this->getSelectOptions($items, $options, $current, $option['id'], $level + 1, $disabled);
        }
    }

    public static function getPath($id, $withOutRoot = false)
    {
        $root = ($withOutRoot ? '' : Yii::getAlias('@project/files/'));

        if ($id === null || $id === 0) {
            return $root;
        }

        $filegroupPaths = Yii::$app->cache->get('filemanager_filegroup_paths'.($withOutRoot?'_with_out_root':''));

        if (!$filegroupPaths) {
            $filegroupPaths = [];
        }

        if (!isset($filegroupPaths[$id])) {
            $filegroup = self::find()->select('name, parent_filegroup_id')->where(['id' => $id])->asArray()->one();
            $target = '';

            if ($filegroup['parent_filegroup_id'] !== null && $filegroup['parent_filegroup_id'] !== '') {
                $target .= self::getPath($filegroup['parent_filegroup_id'], $withOutRoot);
            }

            $filegroupPaths[$id] = $target.($target !== ''?'/':$root).Inflector::slug($filegroup['name']);

            Yii::$app->cache->set('filemanager_filegroup_paths'.($withOutRoot?'_with_out_root':''), $filegroupPaths);
        }

        if (!$withOutRoot && !is_dir($filegroupPaths[$id])) {
            FileHelper::createDirectory($filegroupPaths[$id]);
        }

        return $filegroupPaths[$id];
    }

    public static function getFilegroupStructure($id, $url = null)
    {
        $filegroupStructures = Yii::$app->cache->get('filemanager_filegroup_structures');

        if (!$filegroupStructures) {
            $filegroupStructures = [];
        }

        if (!isset($filegroupStructures[$id])) {
            $filegroup = self::find()->select('id, name, parent_filegroup_id')->where(['id' => $id])->asArray()->one();
            $parent = [];

            if ($filegroup['parent_filegroup_id'] !== null && $filegroup['parent_filegroup_id'] !== '') {
                $parent = self::getFilegroupStructure($filegroup['parent_filegroup_id']);
            }

            $filegroupStructures[$id] = array_merge($parent, [['name' => $filegroup['name'], 'id' => $filegroup['id']]]);

            Yii::$app->cache->set('filemanager_filegroup_structures', $filegroupStructures);
        }

        return $filegroupStructures[$id];
    }

    public static function getBreadcrumb($id, $options = [], $urlStructure = null)
    {
        $structureItems = self::getFilegroupStructure($id);

        $filegroupBreadcrumbs = [];

        foreach ($structureItems as $structureItem) {
            if ($urlStructure === null || !($urlStructure instanceof \Closure)) {
                $url = [self::instance()->getEditData()['url'] . '/index', 'id' => $structureItem['id']];
            } else {
                $url = call_user_func($urlStructure, $structureItem);
            }

            $filegroupBreadcrumbs[] = array_merge(['label' => $structureItem['name'], 'url' => $url], $options);
        }

        return $filegroupBreadcrumbs;
    }

    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }

        foreach ($this->getFiles()->all() as $file) {
            if (!$file->delete()) {
                $this->addError('file', implode('<br><br>', $file->getErrorSummary(true)));
                return false;
            }
        }

        foreach ($this->getFilegroups()->all() as $filegroup) {
            if (!$filegroup->delete()) {
                $this->addError('filegroup', implode('<br><br>', $filegroup->getErrorSummary(true)));
                return false;
            }
        }

        $foldername = self::getPath($this->id);

        try {
            if (file_exists($foldername) && !rmdir($foldername)) {
                $this->addError('folder', 'Cannot delete '.$foldername.'.<br>Please check the file permissions!');
                return false;
            }
        } catch (\Exception $e) {
            $this->addError('folder', 'Cannot delete '.$foldername.'.<br>Please check the file permissions!');
            return false;
        }

        return true;
    }

    public function beforeSave($insert): bool
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        if ((int)$this->parent_filegroup_id === 0) {
            $this->parent_filegroup_id = null;
        }

        return true;
    }

    public function save($runValidation = true, $attributeNames = null): bool
    {
        $changed = $this->isAttributeChanged('name') || $this->isAttributeChanged('parent_filegroup_id');

        if ($changed) {
            $oldPath = self::getPath($this->id);
        }

        if (parent::save($runValidation, $attributeNames)) {
            if ($changed) {
                Yii::$app->cache->delete('filemanager_filegroup_paths');
                Yii::$app->cache->delete('filemanager_filegroup_structures');
                Yii::$app->cache->delete('filemanager_file_paths');

                $newPath = self::getPath($this->id);

                if (is_dir($oldPath)) {
                    @rename($oldPath, $newPath);
                } else {
                    FileHelper::createDirectory($newPath);
                }
            }
        }

        return true;
    }
}
