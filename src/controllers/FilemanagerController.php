<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-filemanager/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-filemanager
 * @see https://www.finally-a-fast.com/packages/fafcms-module-filemanager/docs Documentation of fafcms-module-filemanager
 * @since File available since Release 1.0.0
 */

namespace fafcms\filemanager\controllers;

use fafcms\fafcms\components\FafcmsComponent;
use fafcms\fafcms\components\ViewComponent;
use fafcms\fafcms\models\QueueHelper;

use fafcms\filemanager\{
    Bootstrap,
    jobs\FileToImageJob,
    jobs\VideoConvertToJpgJob,
    jobs\VideoConvertToVideoJob,
    jobs\VideoExtractFramesJob,
    jobs\VideoFramesToAnimationJob,
    models\Archive,
    models\ArchiveForm,
    models\File,
    models\Fileformatvariation,
    models\Filegroup,
    models\FileSelectForm,
    models\Filesizevariation,
    models\Filetype,
    Module
};

use fafcms\helpers\AccessControl;

use fafcms\parser\component\Parser;

use Imagine\{
    Factory\ClassFactory,
    Image\AbstractImagine,
    Image\Box,
    Image\ImageInterface,
    Image\ManipulatorInterface,
    Image\Point,
    Imagick\Imagine
};

use Imagine\Image\Palette\RGB;
use Imagine\Filter\Basic\Autorotate;

use stdClass;

use Yii;

use yii\{
    base\ErrorException,
    db\Expression,
    helpers\ArrayHelper,
    helpers\FileHelper,
    helpers\Inflector,
    helpers\Url,
    web\BadRequestHttpException,
    web\Controller,
    web\ForbiddenHttpException,
    web\HttpException,
    web\NotFoundHttpException,
    web\Response
};

use yii\helpers\Html;

use ZipStream\{
    Exception\FileNotFoundException,
    Exception\FileNotReadableException,
    Option\Method,
    ZipStream
};

/**
 * Class FilemanagerController
 *
 * @package fafcms\filemanager\controllers
 */
class FilemanagerController extends Controller
{
    private const DEBUG = false;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['get-file', 'archive', 'archive-download'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $model = null;
        $id = Yii::$app->request->get('id');

        $this->view->params['breadcrumbs'] = [[
            'label' => Yii::t('fafcms-filemanager', 'Files'),
            'url' => [Filegroup::instance()->getEditData()['url'] . '/index']
        ]];

        $this->view->title = ArrayHelper::getColumn($this->view->params['breadcrumbs'], 'label');

        if ($id !== null) {
            $model = $this->findModel($id);
            Yii::$app->view->params['upload-filegroup'] = $model->id;

            $this->view->params['breadcrumbs'] = array_merge($this->view->params['breadcrumbs'], Filegroup::getBreadcrumb($model->id));
        }

        $this->view->title = ArrayHelper::getColumn($this->view->params['breadcrumbs'], 'label');

        Yii::$app->params['body']['data-fafcms-file-upload-modal-url'] = Url::to(['/' . Bootstrap::$id . '/file/upload', 'filegroup_id' => $model->id??null]);

        $filegroupQuery = Filegroup::find();

        $fileQuery = File::find()->select([
            File::tableName().'.id',
            File::tableName().'.filegroup_id',
            File::tableName().'.filetype_id',
            File::tableName().'.filename',
            File::tableName().'.size',
            File::tableName().'.alt',
            Filetype::tableName().'.mediatype',
            Filetype::tableName().'.mime_type',
            Filetype::tableName().'.default_extension'
        ])
        ->innerJoinWith('filetype', false);

        $files = clone $fileQuery;
        $files = $files->andWhere([File::tableName().'.filegroup_id' => $id])->orderBy(File::tableName().'.filename')->asArray()->all();

        $filegroups = clone $filegroupQuery;
        $filegroups = $filegroups->andWhere(['parent_filegroup_id' => $id])->orderBy('name')->asArray()->all();

        Yii::$app->view->addNavigationItems(FafcmsComponent::NAVIGATION_ACTION_BAR, [[
            'url' => [Filegroup::instance()->getEditData()['url'] . '/create', 'parent_filegroup_id' => $model->id ?? null],
            'icon' => 'folder-plus-outline',
            'label' => Yii::t('fafcms-filemanager', 'Create {modelClass}', [
                'modelClass' => Yii::t('fafcms-filemanager', 'Folder'),
            ])
        ], [
            'url' => Yii::$app->params['body']['data-fafcms-file-upload-modal-url'],
            'icon' => 'upload',
            'label' => Yii::t('fafcms-filemanager', 'Upload file'),
            'options' => [
                'class' => ['ajax-modal primary'],
                'data' => [
                    'ajax-modal-id' => 'fafcms-file-upload-modal',
                ]
            ]
        ]]);

        return $this->render('@fafcms/fafcms/views/common/base', ['content' => $this->renderPartial(
            '@fafcms/fafcms/views/common/rows', [
              'rows' => [[[
                  'content' => $this->renderPartial('@fafcms/fafcms/views/common/card', [
                      'title' => Yii::t('fafcms-filemanager', 'Files'),
                      'content' => $this->renderPartial('index', [
                          'model' => $model,
                          'filegroups' => $filegroups,
                          'files' => $files,
                          'filegroupQuery' => $filegroupQuery,
                          'fileQuery' => $fileQuery,
                      ]),
                  ]),
              ]]],
          ]
        )]);
    }

    public static function getMedia($media, $name, $sizes, $convertSvg = 'false')
    {
        if (!is_array($sizes)) {
            $sizes = [$sizes];
        }

        $sizeParam = '';

        foreach ($sizes as $index => $size) {
            $sizeParam .= ' size-' . $index . '="' . $size . '"';
        }

        return Yii::$app->fafcmsParser->parse(
            Parser::TYPE_PAGE,
            '<'.Yii::$app->fafcmsParser->name.'-media name="'.$name.'" ' . $sizeParam . ' responsive="true" convert-svg="' . $convertSvg . '"><'.Yii::$app->fafcmsParser->name.'-media-src><'.Yii::$app->fafcmsParser->name.'-placeholder attribute="media" /></'.Yii::$app->fafcmsParser->name.'-media-src></'.Yii::$app->fafcmsParser->name.'-media>',
            Parser::ROOT,
            ['media' => $media]
        );
    }

    /**
     * @param string $fileIds
     * @param int    $limit
     * @param string $fileSelectId
     *
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public static function getFileItems(string $fileIds, int $limit, string $fileSelectId): string
    {
        $itemClasses = ['sixteen wide mobile eight wide tablet four wide computer two wide large screen one wide widescreen'];

        if ($limit === 1) {
            $itemClasses = '';
        }

        $files = File::find()->select([
                File::tableName().'.id',
                File::tableName().'.filegroup_id',
                File::tableName().'.filetype_id',
                File::tableName().'.filename',
                File::tableName().'.size',
                File::tableName().'.alt',
                Filetype::tableName().'.mediatype',
                Filetype::tableName().'.mime_type',
                Filetype::tableName().'.default_extension'
            ])
            ->innerJoinWith('filetype', false)
            ->andWhere([File::tableName().'.id' => explode(',', $fileIds)])->orderBy(File::tableName() . '.filename')->asArray()->all();

        $columns = [];

        foreach ($files as $file) {
            $fileName = htmlentities(File::getFileName($file));

            $item = Html::beginTag('div', [
                'class' => [
                    'filemanager-list-item-view',
                ],
            ]);

            if ($file['mediatype'] === 'image') {
                $item .= File::getPicture($file, 'Filemanager preview', [
                    [500, 500, 'bestfit:true;'],
                    [75, 75, 'bestfit:true;']
                ], false, null);
            } else {
                $item .= Html::tag('div', '<i class="mdi mdi-file"></i>', ['class' => 'filemanager-list-item-icon']);
            }

            $item .= Html::endTag('div');

            $item .= Yii::$app->view->render('@fafcms/filemanager/views/filemanager/_item', [
                'name' => $fileName,
                'badges' => [[
                    'icon' => 'harddisk',
                    'count' => Yii::$app->formatter->asShortSize($file['size'], 2)
                ], [
                    'icon' => 'file-question-outline',
                    'count' => $file['default_extension']
                ]],
                'removeLink' => [
                    'class' => 'fafcms-file-select-remove',
                    'data-fafcms-file-select-file' => $file['id'],
                    'data-fafcms-file-select-id' => $fileSelectId,
                ],
                'editLink' => [File::instance()->getEditDataUrl() . '/update', 'id' => $file['id']],
                'deleteLink' => [File::instance()->getEditDataUrl() . '/delete', 'id' => $file['id']],
                'downloadLink' => [Filegroup::instance()->getEditDataUrl() . '/get-file', 'id' => $file['id'], 'download' => 'true'],
            ]);

            $options = [
                'class' => 'ui segment center aligned filemanager-list-item'
            ];

            $columns[] =[
                'options' => ['class' => $itemClasses],
                'content' => Html::tag('div', $item, $options ?? []),
            ];
        }

        return Yii::$app->view->render(
            '@fafcms/fafcms/views/common/grid', [
                'rows' => [$columns],
            ]
        );
    }

    /**
     * @param string $fileIds
     * @param int    $limit
     * @param string $fileSelectId
     *
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function actionGetFileItems(string $fileIds, int $limit, string $fileSelectId): string
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        return self::getFileItems($fileIds, $limit, $fileSelectId);
    }

    public function actionArchiveDownload($id = null)
    {
        if (($archive = Archive::find()->where(['hashId' => $id])->one()) === null) {
            throw new NotFoundHttpException('Archive not found.');
        }

        $name = Inflector::slug(Yii::$app->fafcms->getCurrentProject()->name).'-'.date('d-m-Y').'.zip';

        if ($archive->cron === 0) {
            $archiveFiles = json_decode($archive->files);

            $options = new \ZipStream\Option\Archive();
            $options->setSendHttpHeaders(true);
            $options->setEnableZip64(false);

            $zip = new ZipStream($name, $options);

            foreach ($archiveFiles as $archiveFile) {
                $file = File::find()
                    ->innerJoinWith('filetype', false)
                    ->where([
                        File::tableName().'.id' => $archiveFile->id
                    ])
                    ->one();

                if ($file === null) {
                    continue;
                }

                $fileName = File::getFilePath($file);

                if (($file['is_public'] === '0' && Yii::$app->user->isGuest) || $file['allow_download'] === '0') {
                    continue;
                }

                if (empty($archiveFile->type??null)) {
                    try {
                        $opt = new \ZipStream\Option\File();
                        $opt->setMethod(Method::STORE());
                        $zip->addFileFromPath(File::getFileName($file), $fileName, $opt);
                    } catch (FileNotFoundException $e) {
                    } catch (FileNotReadableException $e) {
                    }
                } else {
                    $fileVariation = self::getFileVariation($archiveFile->type, $file);

                    if ($fileVariation['filesizevariation']['allow_download'] === 0) {
                        continue;
                    }

                    $attachmentName = $file['filename'];

                    if (!empty(($archiveFile->name??null))) {
                        $attachmentName .= '-' . Inflector::slug($archiveFile->name);
                    } else {
                        $meta = File::loadMeta($file, $fileVariation);
                        $filetype = Filetype::find()->where(['id' => $meta['filetypeId']])->one();

                        if ($filetype !== null && $filetype->mediatype === 'image') {
                            if (isset($meta['width'], $meta['height'])) {
                                $attachmentName .= '-' . $meta['width'] . 'x' . $meta['height'];
                            }
                        }
                    }

                    try {
                        $opt = new \ZipStream\Option\File();
                        $opt->setMethod(Method::STORE());
                        $zip->addFileFromPath($attachmentName . '.' . $fileVariation['extension'], $fileVariation['name'], $opt);
                    } catch (FileNotFoundException $e) {
                    } catch (FileNotReadableException $e) {
                    }
                }
            }

            $zip->finish();

            Yii::$app->response->format = Response::FORMAT_RAW;
            Yii::$app->response->isSent = true;
            Yii::$app->end();
        } else {
            if ($archive->generated === 2) {
                return $this->sendFile($archive->getFileName(), $name, []);
            }

            throw new NotFoundHttpException('Ihr Download wird gerade vorbereitet.');
        }
    }

    public function actionArchive()
    {
        $model = new ArchiveForm();

        return Yii::$app->fafcms->handleAjaxForm(
            $model,
            [
                'type' => 'error',
                'message' => static function($model) {
                    return ($model->hasErrors()?implode('<br>', $model->getErrorSummary(true)):'Ihr ZIP-Archiv konnte nicht erstellt werden.<br>Bitte prüfen Sie Ihre Eingaben und versuchen Sie es erneut.');
                }
            ],
            [
                'type' => 'success',
                'message' => static function($model) {
                    if ($model->archive->cron !== 1) {
                        $url = Url::to(['/assets/archive-download', 'id' => $model->archive->hashId]);

                        return 'Ihr ZIP-Archiv wurde erstellt. Sollte der Download nicht automatisch starten <a href="'.$url.'">klicken Sie bitte hier</a>.';
                    }

                    return 'Ihr ZIP-Archiv wird aufgrund der Dateigröße gerade vorbereitet. Sobald das Archiv zur Verfügung steht, werden Sie per E-Mail benachrichtigt.';
                },
                'url' => static function($model) {
                    if ($model->archive->cron !== 1) {
                        return Url::to(['/assets/archive-download', 'id' => $model->archive->hashId]);
                    }

                    return null;
                },
            ],
            null,
            [],
            'save',
            null,
            true
        );
    }

    public function actionSelect()
    {
        Yii::$app->view->renderedSiteMode = 'ajax';

        $model = new FileSelectForm();
        $model->load(Yii::$app->request->get());

        $title = Yii::t('fafcms-filemanager', 'Select file');

        $this->view->params['breadcrumbs'][] = [
            'label' => Yii::t('fafcms-filemanager', 'Root'),
            'divider' => '',
            'options' => [
                'class' => 'ajax-modal',
                'data' => [
                    'ajax-modal-id' => 'fafcms-file-select-modal',
                ]
            ],
            'url' => [
                '/' . Bootstrap::$id . '/filemanager/select',
                'FileSelectForm[id]' => $model->id,
                'FileSelectForm[file]' => $model->file,
                'FileSelectForm[mediatypes]' => $model->mediatypes,
                'FileSelectForm[mimetypes]' => $model->mimetypes,
                'FileSelectForm[limit]' => $model->limit,
                'FileSelectForm[external]' => $model->external,
                'FileSelectForm[filegroup]' => null,
            ]
        ];

        if ($model->filegroup === 0) {
            $model->filegroup = null;
        }

        if ($model->filegroup !== null) {
            $filegroup = $this->findModel($model->filegroup);

            $this->view->params['breadcrumbs'] = array_merge($this->view->params['breadcrumbs'], Filegroup::getBreadcrumb($filegroup->id, [
                'options' => [
                    'class' => 'ajax-modal',
                    'data' => [
                        'ajax-modal-id' => 'fafcms-file-select-modal',
                    ]
                ]
            ], function($structureItem) use ($model) {
                return [
                    '/' . Bootstrap::$id . '/filemanager/select',
                    'FileSelectForm[id]' => $model->id,
                    'FileSelectForm[file]' => $model->file,
                    'FileSelectForm[mediatypes]' => $model->mediatypes,
                    'FileSelectForm[mimetypes]' => $model->mimetypes,
                    'FileSelectForm[limit]' => $model->limit,
                    'FileSelectForm[external]' => $model->external,
                    'FileSelectForm[filegroup]' => $structureItem['id'],
                ];
            }));
        }

        $this->view->title = $title;

        $fileQuery = File::find()->select([
            File::tableName().'.id',
            File::tableName().'.filegroup_id',
            File::tableName().'.filetype_id',
            File::tableName().'.filename',
            File::tableName().'.size',
            File::tableName().'.alt',
            Filetype::tableName().'.mediatype',
            Filetype::tableName().'.mime_type',
            Filetype::tableName().'.default_extension'
        ])
        ->innerJoinWith('filetype', false);

        $filegroupQuery = Filegroup::find();

        $files = clone $fileQuery;
        $files = $files->andWhere([File::tableName() . '.filegroup_id' => $model->filegroup])->orderBy(File::tableName().'.filename')->asArray()->all();

        $filegroups = clone $filegroupQuery;
        $filegroups = $filegroups->andWhere(['parent_filegroup_id' => $model->filegroup])->orderBy('name')->asArray()->all();

        $buttons = static function ($model) {
            return [[
                'icon' => 'close',
                'label' => Yii::t('fafcms-filemanager', 'Abort'),
                'type' => 'button',
                'options' => [
                    'class' => 'cancel',
                ]
            ], [
                'icon' => 'upload',
                'label' => Yii::t('fafcms-filemanager', 'Upload file'),
                'url' => Yii::$app->params['body']['data-fafcms-file-upload-modal-url'],
                'options' => [
                    'class' => 'ajax-modal',
                    'data' => [
                        'ajax-modal-id' => 'fafcms-file-upload-modal',
                    ]
                ],
            ], [
                'icon' => 'image-move',
                'label' => Yii::t('fafcms-filemanager', ($model->limit === 1 ? 'Apply' : 'Add')),
                'type' => 'button',
                'options' => [
                    'class' => 'fafcms-file-select-apply primary',
                    'data-fafcms-file-select-id' => $model->id,
                    'data-fafcms-file-select-file' => $model->file,
                    'data-fafcms-file-select-url' => Url::to([
                        '/' . Bootstrap::$id . '/filemanager/select',
                        'FileSelectForm[id]' => $model->id,
                        'FileSelectForm[file]' => $model->file,
                        'FileSelectForm[mediatypes]' => $model->mediatypes,
                        'FileSelectForm[mimetypes]' => $model->mimetypes,
                        'FileSelectForm[limit]' => $model->limit,
                        'FileSelectForm[external]' => $model->external,
                        'FileSelectForm[filegroup]' => $model->filegroup,
                    ])
                ],
            ]];
        };

        $content = [
            'model' => $model,
            'buttons' => $buttons,
            'buttonCard' => true,
            'buttonCardOptions' => ['cardActionOptions' => ['class' => 'hide']],
            'formOptions' => [
                'id' => 'fafcms-file-select-form',
                'action' => ['#'],
                'method' => 'get'
            ],
            'content' => function ($model, $form, $view) use ($title, $buttons, $filegroups, $files, $filegroupQuery, $fileQuery) {
                $breadcrumb = Yii::$app->view->params['breadcrumbs'];
                $homeLink = array_splice($breadcrumb, 0, 1)[0];

                return $view->render('/filemanager/index', [
                    'select' => true,
                    'model' => $model,
                    'form' => $form,
                    'filegroups' => $filegroups,
                    'files' => $files,
                    'filegroupQuery' => $filegroupQuery,
                    'fileQuery' => $fileQuery,
                    'title' => $title,
                    'breadcrumb' => $breadcrumb ?? null,
                    'homeLink' => $homeLink ?? null,
                ]) . $view->render('@fafcms/fafcms/views/common/buttons', [
                    'buttons' => $buttons,
                    'model' => $model,
                    'form' => $form,
                ]);
            },
        ];

        return $this->renderAjax('@fafcms/fafcms/views/common/edit', $content);
    }

    public function actionDeleteFileVariations($id)
    {
        $fileObj = File::find()->select([
                 File::tableName().'.id',
                 File::tableName().'.allow_download',
                 File::tableName().'.is_public',
                 File::tableName().'.filegroup_id',
                 File::tableName().'.filetype_id',
                 File::tableName().'.filename',
                 Filetype::tableName().'.mediatype',
                 Filetype::tableName().'.mime_type',
                 Filetype::tableName().'.default_extension'
             ])
            ->innerJoinWith('filetype', false)
            ->where([
                File::tableName().'.id' => $id
            ])
            ->asArray()
            ->one();

        if ($fileObj === null) {
            return 'nope';
        }

        foreach (glob(File::getFilePath($fileObj, $fileObj['filename'].'-FAFCMS-GENERATED-*', true)) as $file) {
            try {
                if (file_exists($file) && !unlink($file)) {
                    return 'Cannot delete '.$file.'.<br>Please check the file permissions!';
                }
            } catch (\Exception $e) {
                return 'Cannot delete '.$file.'.<br>Please check the file permissions!';
            }
        }

        return 'Deleted file variations for file: '.File::getFilePath($fileObj);
    }

    /**
     * Deletes an existing File model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $fileGroup = $this->findModel($id);

        if ($fileGroup->delete()) {
            Yii::$app->session->setFlash('success', Yii::t('fafcms-filemanager', '{modelClass} has been deleted!', [
                'modelClass' => $fileGroup->getEditData()['singular'],
            ]));
        } else {
            Yii::$app->session->setFlash('error', Yii::t('fafcms-filemanager', 'Error while deleting {modelClass}!', [
                'modelClass' => $fileGroup->getEditData()['singular']
            ]).'<br><br>'.implode('<br><br>', $fileGroup->getErrorSummary(true)));
        }

        return $this->redirect([Filegroup::instance()->getEditData()['url'] . '/index', 'id' => $fileGroup->parent_filegroup_id]);
    }

    public function actionViewPdf()
    {
        $this->layout = 'pdf';
        return $this->renderContent('');
    }

    /**
     * @return string
     */
    public static function getProcessingFile(): string
    {
        $processing = Yii::getAlias('@fafcms/filemanager/assets/processing/');

        $processingFile = $processing . Yii::$app->language . '.jpg';

        if (!file_exists($processingFile)) {
            $processingFile = $processing . explode('_', Yii::$app->language)[0] . '.jpg';

            if (!file_exists($processingFile)) {
                $processingFile = $processing . 'en.jpg';
            }
        }

        return $processingFile;
    }

    public function actionGetFile($id)
    {
        $file = File::find()->select([
                File::tableName().'.id',
                File::tableName().'.allow_download',
                File::tableName().'.is_public',
                File::tableName().'.filegroup_id',
                File::tableName().'.filetype_id',
                File::tableName().'.filename',
                Filetype::tableName().'.mediatype',
                Filetype::tableName().'.mime_type',
                Filetype::tableName().'.default_extension'
            ])
            ->innerJoinWith('filetype', false)
            ->where([
                File::tableName().'.id' => $id
            ])
            ->asArray()
            ->one();

        if ($file === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $download = Yii::$app->request->get('download', 'false') === 'true';

        $options = $download ? [] : ['inline' => true];

        if (mb_stripos($_SERVER['SERVER_SOFTWARE'], 'nginx') !== false) {
            $options['xHeader'] = 'X-Accel-Redirect';
        }

        $type = Yii::$app->request->get('type', null);

        $fileName = File::getFilePath($file);

        if ($type === null) {
            if ($file['is_public'] === '0' && Yii::$app->user->isGuest) {
                return Yii::$app->user->loginRequired();
            }

            if ($download && $file['allow_download'] === '0') {
                throw new ForbiddenHttpException('Download is not allowed for this file.');
            }

            return Yii::$app->response->sendFile($fileName, File::getFileName($file), $options);
        }

        $module = Yii::$app->getModule(Bootstrap::$id);
        $validationKey = $module->validationKey;

        if ($validationKey === null) {
            throw new InvalidConfigException(get_class($module) . '::validationKey must be configured with a secret key.');
        }

        $type = Yii::$app->security->validateData($type, $validationKey);

        if ($type === false) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $typeData = json_decode(base64_decode($type));

        try {
            $fileVariation = self::getFileVariation($typeData, $file);
        } catch (ForbiddenHttpException $e) {
            return Yii::$app->user->loginRequired();
        }

        if ($fileVariation === null) {
            if (Module::getLoadedModule()->getPluginSettingValue('generate_images_on_the_fly') === 0) {
                return Yii::$app->response->sendFile(self::getProcessingFile(), 'processing.jpg', $options);
            }

            throw new HttpException(503, 'Service Unavailable');
        }

        if ($download && $fileVariation['filesizevariation']['allow_download'] === 0) {
            throw new ForbiddenHttpException('Download is not allowed for this file.');
        }

        $attachmentName = $file['filename'];

        if ($fileVariation['pageNumber'] !== null) {
            $attachmentName .= '-'.Inflector::slug(Yii::t('fafcms-filemanager', 'Page')).'-'.($fileVariation['pageNumber'] + 1);
        }

        return Yii::$app->response->sendFile($fileVariation['name'], $attachmentName.'.'.$fileVariation['extension'], $options);
    }

    /**
     * @param array $typeData
     * @param       $file
     * @param false $onTheFly
     *
     * @return array|null
     * @throws ErrorException
     * @throws ForbiddenHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public static function getFileVariation(array $typeData, $file, $onTheFly = false): ?array
    {
        if ($file instanceof File) {
            $fileModel = $file;
            $file = $fileModel->getAttributes([
                'id',
                'status',
                'display_start',
                'display_end',
                'filegroup_id',
                'filetype_id',
                'filename',
                'size',
                'meta',
                'allow_download',
                'is_public',
            ]);
            $file['default_extension'] = $fileModel->filetype->default_extension;
            $file['mediatype'] = $fileModel->filetype->mediatype;
            $file['mime_type'] = $fileModel->filetype->mime_type;
        }

        $fileName = File::getFilePath($file);

        $imageOptions = Html::cssStyleToArray($typeData[3]);
        unset($imageOptions['media']);
        $cleanVariation = Html::cssStyleFromArray($imageOptions);

        if (empty($typeData[0])) {
            $typeData[0] = 'default';
        }

        $pageNumber = 0;

        if (isset($imageOptions['pageNumber'])) {
            $pageNumber = (int)$imageOptions['pageNumber'];
        }

        $filesizevariation = Filesizevariation::find()->where([
            'file_id' => $file['id'],
            'name' => $typeData[0],
            'height' => $typeData[1],
            'width' => $typeData[2]
        ])->one();

        if ($filesizevariation === null) {
            $filesizevariation = new Filesizevariation([
                'file_id' => $file['id'],
                'name' => $typeData[0],
                'height' => $typeData[1],
                'width' => $typeData[2],
                'allow_download' => $file['allow_download'],
                'is_public' => $file['is_public'],
                'last_request_at' => (new \DateTime('NOW', new \DateTimeZone(Yii::$app->formatter->defaultTimeZone)))->format('Y-m-d H:i:s'),
                'fafcmsLogChange' => false
            ]);

            if (!$filesizevariation->save()) {
                throw new ErrorException('Cannot create file size variation.');
            }
        } else {
            $filesizevariation->last_request_at = (new \DateTime('NOW', new \DateTimeZone(Yii::$app->formatter->defaultTimeZone)))->format('Y-m-d H:i:s');
            $filesizevariation->fafcmsLogChange = false;
            $filesizevariation->save(false);
        }

        if ($filesizevariation['is_public'] === 0 && Yii::$app->user->isGuest) {
            throw new ForbiddenHttpException(Yii::t('yii', 'Login Required'));
        }

        $fileformatvariation = Fileformatvariation::find()->where([
            'filesizevariation_id' => $filesizevariation->id,
            'variation' => $cleanVariation ?? 'default'
        ])->one();

        if ($fileformatvariation === null) {
            $fileformatvariation = new Fileformatvariation([
                'filesizevariation_id' => $filesizevariation->id,
                'variation' => $cleanVariation ?? 'default',
                'last_request_at' => (new \DateTime('NOW', new \DateTimeZone(Yii::$app->formatter->defaultTimeZone)))->format('Y-m-d H:i:s'),
                'fafcmsLogChange' => false
            ]);

            if (!$fileformatvariation->save()) {
                throw new ErrorException('Cannot create file format variation.');
            }
        } else {
            $fileformatvariation->last_request_at = (new \DateTime('NOW', new \DateTimeZone(Yii::$app->formatter->defaultTimeZone)))->format('Y-m-d H:i:s');
            $fileformatvariation->fafcmsLogChange = false;
            $fileformatvariation->save(false);
        }

        $sourceFormat = $file['default_extension'];

        if ($sourceFormat === 'pdf') {
            $imageOptions['format'] = 'jpg';
        }

        if ($file['mediatype'] === 'video') {
            $imageOptions['format'] = 'mp4';

            if (isset($imageOptions['type'])) {
                $imageOptions['format'] = 'jpg';

                if ($imageOptions['type'] === 'animation') {
                    $imageOptions['format'] = 'gif';
                }
            }
        }

        if (isset($imageOptions['format'])) {
            $file['default_extension'] = $imageOptions['format'];
        } elseif ($file['mediatype'] === 'image') {
            $webFormats = [
                'jpg',
                'png',
                'gif',
                'webp',
            ];

            if (!in_array($file['default_extension'], $webFormats)) {
                $file['default_extension'] = 'jpg';
            }
        }

        $fileFormatName = File::getFilePath($file, $file['filename'].'-FAFCMS-GENERATED-'.$filesizevariation->id.'-'.$fileformatvariation->id);

        if (self::DEBUG || !file_exists($fileFormatName)) {
            $generated = false;

            if ($sourceFormat === 'pdf') {
                $generated = self::convertImage($fileName.'['.$pageNumber.']', $fileFormatName, $sourceFormat, $typeData, $imageOptions, $onTheFly);
            } elseif (($file['mediatype'] === 'image')) {
                $generated = self::convertImage($fileName, $fileFormatName, $sourceFormat, $typeData, $imageOptions, $onTheFly);
            } elseif (($file['mediatype'] === 'video')) {
                if ($imageOptions['format'] === 'jpg') {
                    $rawFileFormatName = self::getRawImageFromVideo($file, $imageOptions);
                    $generated = self::convertImage($rawFileFormatName, $fileFormatName, $sourceFormat, $typeData, $imageOptions);
                } elseif ($imageOptions['format'] === 'gif') {
                    return null;
                } else {
                    var_dump($imageOptions);die();
                    QueueHelper::runJob(VideoConvertToVideoJob::class, [
                        'fileId' => $file['id'],
                        'fileFormatName' => $fileFormatName,
                        'typeData' => $typeData,
                        'imageOptions' => $imageOptions,
                    ]);

                    return null;
                }
            }

            if (self::DEBUG === false && $onTheFly === false && Module::getLoadedModule()->getPluginSettingValue('generate_images_on_the_fly') === 0) {
                return null;
            }

            if (!$generated) {
                throw new ErrorException('Cannot generate thumbnail for file "'.$file['id'].'".');
            }
        }

        return [
            'name' => $fileFormatName,
            'filesizevariation' => $filesizevariation,
            'fileformatvariation' => $fileformatvariation,
            'extension' => $file['default_extension'],
            'pageNumber' => ($sourceFormat === 'pdf' ? $pageNumber : null)
        ];
    }

    public static function convertImage(string $fileName, string $fileFormatName, string $sourceFormat, array $type, array $imageOptions, bool $onTheFly = false): bool
    {
        if (self::DEBUG === false && $onTheFly === false && Module::getLoadedModule()->getPluginSettingValue('generate_images_on_the_fly') === 0) {
            return QueueHelper::runJob(FileToImageJob::class, [
                'fileName' => $fileName,
                'fileFormatName' => $fileFormatName,
                'sourceFormat' => $sourceFormat,
                'type' => $type,
                'imageOptions' => $imageOptions
            ]);
        }

        return self::generateImage($fileName, $fileFormatName, $sourceFormat, $type, $imageOptions);
    }

    public static function getRawAnimationFromVideo($file, $imageOptions)
    {
        $file['default_extension'] = 'vtt';
        $rawFileVttFormatName = File::getFilePath($file, $file['filename'].'-FAFCMS-GENERATED-VTT');

        $file['default_extension'] = 'jpg';
        $rawFileVttImageFormatName = File::getFilePath($file, $file['filename'].'-FAFCMS-GENERATED-VTT-IMAGE');

        $file['default_extension'] = 'gif';
        $rawFileGifFormatName = File::getFilePath($file, $file['filename'].'-FAFCMS-GENERATED-ANIMATION-RAW');

        $rawFileWebpFormatName = null;

        if (Yii::$app->getModule(Bootstrap::$id)->webp) {
            $file['default_extension'] = 'webp';
            $rawFileWebpFormatName = File::getFilePath($file, $file['filename'] . '-FAFCMS-GENERATED-ANIMATION-RAW');
        }

        $framePath = File::getFileTarget($file).'/.'.$file['filename'].'-FAFCMS-GENERATED-FRAMES';

        if (!FileHelper::createDirectory($framePath)) {
            return $rawFileGifFormatName;
        }

        if (!file_exists($framePath.'/frame-2.jpg') || !file_exists($rawFileVttFormatName) || !file_exists($rawFileVttImageFormatName)) {
            QueueHelper::runJob(VideoExtractFramesJob::class, [
                'fileId' => $file['id'],
                'framePath' => $framePath,
                'rawFileVttFormatName' => $rawFileVttFormatName,
                'rawFileVttImageFormatName' => $rawFileVttImageFormatName
            ]);

            return $rawFileGifFormatName;
        }

        if (!file_exists($rawFileGifFormatName) || (Yii::$app->getModule(Bootstrap::$id)->webp && !file_exists($rawFileWebpFormatName))) {
            QueueHelper::runJob(VideoFramesToAnimationJob::class, [
                'framePath' => $framePath,
                'rawFileWebpFormatName' => $rawFileWebpFormatName,
                'rawFileGifFormatName' => $rawFileGifFormatName
            ]);
        }

        return $rawFileGifFormatName;
    }

    public static function getRawImageFromVideo($file, $imageOptions)
    {
        $timeCode = $imageOptions['timecode'] ?? 2;
        $rawFileFormatName = File::getFilePath($file, $file['filename'] . '-FAFCMS-GENERATED-JPG-RAW-' . $timeCode, true) . '.jpg';

        if (!file_exists($rawFileFormatName)) {
            QueueHelper::runJob(VideoConvertToJpgJob::class, [
                'fileId' => $file['id'],
                'rawFileFormatName' => $rawFileFormatName,
                'timeCode' => $timeCode
            ]);
        }

        return $rawFileFormatName;
    }
//    public function getFFMpeg()
//    {
//        return FFMpeg::create([
//            'ffmpeg.binaries'  => '/usr/local/bin/ffmpeg',
//            'ffprobe.binaries' => '/usr/local/bin/ffprobe',
//            'ffmpeg.threads'   => 16
//        ]);
//    }

    /**
     * @param ManipulatorInterface $image
     *
     * @return string
     */
    private static function getFilter(ManipulatorInterface $image): string
    {
        if ($image instanceof \Imagine\Gd\Image) {
            return ImageInterface::FILTER_UNDEFINED;
        }

        return ImageInterface::FILTER_LANCZOS;
    }

    /**
     * @param string $fileName
     * @param string $fileFormatName
     * @param string $sourceFormat
     * @param array  $type
     * @param array  $imageOptions
     *
     * @return bool
     * @throws ErrorException
     * @throws \ImagickException
     */
    public static function generateImage(string $fileName, string $fileFormatName, string $sourceFormat, array $type, array $imageOptions): bool
    {
        if (!isset($imageOptions['format'])) {
            $imageOptions['format'] = $sourceFormat;
        }

        if (!isset($imageOptions['background'])) {
            $imageOptions['background'] = '#ffffff';

            if (!isset($imageOptions['background-opacity'])) {
                $imageOptions['background-opacity'] = 0;
            }
        } elseif (!isset($imageOptions['background-opacity'])) {
            $imageOptions['background-opacity'] = 100;
        }

        $imagine = Module::getLoadedModule()->getImagine();
        $contentImage = self::getImage($imagine, $fileName, $sourceFormat);
        $image = $imagine->create($contentImage->getSize(), new \Imagine\Image\Palette\Color\RGB(new RGB(), [0, 0, 0], 0));

        if (empty($imageOptions['inner-background-image'])) {
            $imageOptions['inner-background-image'] = Module::getLoadedModule()->getPluginSettingValue('default_thumbnail_inner_background_image');
        }

        if (isset($imageOptions['inner-background-image']) && !empty($imageOptions['inner-background-image'])) {
            $fileData = self::getFileData($imageOptions['inner-background-image']);
            $backgroundImage = self::getImage($imagine, $fileData['fileName'] ?? $fileName, $fileData['fileFormat'] ?? $sourceFormat);

            if (($imageOptions['inner-background-image-blur'] ?? 'false') === 'true') {
                $backgroundImage->effects()->blur($imageOptions['inner-background-image-blur-sigma'] ?? 10);
            }

            $backgroundImage->resize($contentImage->getSize(), self::getFilter($backgroundImage));

            $image->paste($backgroundImage, new Point(0, 0));
        }

        $image->paste($contentImage, new Point(0, 0));

        if (false && $sourceFormat === 'gif') {
            $image = $image->coalesceImages();

            do {
                self::formatImage($imagine, $image, $sourceFormat, $type, $imageOptions, $fileName);
            } while ($image->nextImage());

            //$image = $image->optimizeImageLayers();
            $image = $image->deconstructImages();
            $result = $image->writeImages($fileFormatName, true);
        } else {
            self::formatImage($imagine, $image, $sourceFormat, $type, $imageOptions, $fileName);

            $imageSaveOptions = [
                'quality' => 90
            ];

            /*->save('/path/to/image.jpg', array('jpeg_quality' => 50)) // from 0 to 100
            ->save('/path/to/image.png', array('png_compression_level' => 9)); // from 0 to 9
            ->save('/path/to/image.webp', array('webp_quality' => 50)) // from 0 to 100*/

            $image->save($fileFormatName, $imageSaveOptions);
            $result = true;
        }

        return $result;
    }

    /**
     * @param string $src
     *
     * @return array|null
     */
    public static function getFileData(string $src): ?array
    {
        if ($src !== 'self') {
            $file = File::find()->select([
                    File::tableName().'.id',
                    File::tableName().'.allow_download',
                    File::tableName().'.is_public',
                    File::tableName().'.filegroup_id',
                    File::tableName().'.filetype_id',
                    File::tableName().'.filename',
                    Filetype::tableName().'.mediatype',
                    Filetype::tableName().'.mime_type',
                    Filetype::tableName().'.default_extension'
                ])
                ->innerJoinWith('filetype', false)
                ->where([
                    File::tableName().'.id' => $src
                ])
                ->asArray()
                ->one();

            return [
                'fileName' => File::getFilePath($file),
                'fileFormat' => $file['default_extension']
            ];
        }

        return null;
    }

    /**
     * @param AbstractImagine $imagine
     * @param string          $fileName
     * @param string          $fileFormat
     *
     * @return ManipulatorInterface
     * @throws \ImagickException
     */
    public static function getImage(AbstractImagine $imagine, string $fileName, string $fileFormat): ManipulatorInterface
    {
        if ($fileFormat === 'pdf' && $imagine instanceof Imagine) {
            $imagickImage = new \Imagick();
            $imagickImage->setType(\Imagick::IMGTYPE_TRUECOLORMATTE);
            $imagickImage->setColorspace(\Imagick::COLORSPACE_RGB);
            $imagickImage->readImage($fileName);
            $imagickImage->setImageFormat('jpg');

            $image = $imagine->load($imagickImage->getImageBlob());
        } elseif ($fileFormat === 'svg' && $imagine instanceof Imagine) {
            $pixel = new \ImagickPixel('transparent');

            $imagickImage = new \Imagick();
            $imagickImage->setBackgroundColor($pixel);
            $imagickImage->readImage($fileName);
            $imagickImage->setImageFormat('png32');

            $loader = $imagine->getClassFactory()->createFileLoader($fileName);
            $image = $imagine->getClassFactory()->createImage(ClassFactory::HANDLE_IMAGICK, $imagickImage, new RGB(), $imagine->getMetadataReader()->readFile($loader));
        } else {
            $image = $imagine->open($fileName);
        }

        $autorotate = new Autorotate();
        $autorotate->apply($image);

        $image->usePalette(new RGB());

        return $image;
    }

    /**
     * @param AbstractImagine      $imagine
     * @param ManipulatorInterface $image
     * @param string               $sourceFormat
     * @param array                $type
     * @param array                $imageOptions
     * @param string               $fileName
     *
     * @return bool|null
     * @throws \ImagickException
     */
    public static function formatImage(AbstractImagine $imagine, ManipulatorInterface &$image, string $sourceFormat, array $type, array $imageOptions, string $fileName): ?bool
    {
        $bestfit = ($imageOptions['bestfit']??'false') === 'true';
        $shrink = ($imageOptions['shrink']??'false') === 'true';
        $extend = ($imageOptions['extend']??'false') === 'true';

        if ($bestfit || $shrink) {
            $filter = self::getFilter($image);

            if ($bestfit) {
                $image = $image->thumbnail(new Box($type[1], $type[2]), ImageInterface::THUMBNAIL_INSET, $filter);
            } else {
                $image = $image->resize(new Box($type[1], $type[2]), $filter);
            }

            if ($extend) {
                $old = $image;

                $image = $imagine->create(new Box($type[1], $type[2]), $old->palette()->color($imageOptions['background'], (int)$imageOptions['background-opacity']));

                if (empty($imageOptions['background-image'])) {
                    $imageOptions['background-image'] = Module::getLoadedModule()->getPluginSettingValue('default_thumbnail_background_image');
                }

                if (isset($imageOptions['background-image']) && !empty($imageOptions['background-image'])) {
                    $fileData = self::getFileData($imageOptions['background-image']);
                    $backgroundImage = self::getImage($imagine, $fileData['fileName'] ?? $fileName, $fileData['fileFormat'] ?? $sourceFormat);

                    self::cropImage($imagine, $backgroundImage, $type[1],  $type[2], [
                        'background' => '#ffffff',
                        'background-opacity' => 0,
                    ], null);

                    if (($imageOptions['background-image-blur']??'false') === 'true') {
                        $backgroundImage->effects()->blur($imageOptions['background-image-blur-sigma']??10);
                    }

                    $image->paste($backgroundImage, new Point(0, 0));
                }

                $image->paste($old, new Point(($type[1] - $old->getSize()->getWidth()) / 2, ($type[2] - $old->getSize()->getHeight()) / 2));
            }
        } else {
            self::cropImage($imagine, $image, $type[1],  $type[2], $imageOptions, null);
        }

        if (($imageOptions['blur']??'false') === 'true') {
            $image->effects()->blur($imageOptions['blur-sigma']??10);
        }

        $image->strip();
        $image->interlace(ImageInterface::INTERLACE_PLANE);

        try {
           // $image->setImageFormat($imageOptions['format']??$sourceFormat);
        } catch (\Exception $exception) {
            return false;
        }

        return true;
    }

    /**
     * @param AbstractImagine $imagine
     * @param ManipulatorInterface  $image
     * @param int             $width
     * @param int             $height
     * @param array           $imageOptions
     * @param stdClass|null   $settings
     */
    public static function cropImage(AbstractImagine $imagine, ManipulatorInterface &$image, int $width, int $height, array $imageOptions, ?stdClass $settings): void
    {
        $size_w = $image->getSize()->getWidth();
        $size_h = $image->getSize()->getHeight();
        $aspect = $width / $height;

        if (!isset($settings)) {
            $initialData = new stdClass();
            $initialData->x = ($size_w - ($size_h * $aspect)) / 2;
            $initialData->y = 0;
            $initialData->width = $size_h * $aspect;
            $initialData->height = $size_h;

            if ($initialData->x  < 0) {
                $initialData->x = 0;
                $initialData->y = ($size_h - ($size_w / $aspect)) / 2;
                $initialData->width = $size_w;
                $initialData->height = $size_w * $aspect;
            }

            $initialData->scaleX = 1;
            $initialData->scaleY = 1;

            $palette = new RGB();
            $settings = new stdClass();
            $settings->color = $palette->color($imageOptions['background'], (int)$imageOptions['background-opacity']);
            $settings->positions = $initialData;
        }

        $src_img_w = $size_w;
        $src_img_h = $size_h;
        $tmp_img_w = $settings->positions->width;
        $tmp_img_h = $settings->positions->height;
        $dst_img_w = $width;
        $dst_img_h = $width / $aspect;

        $src_x = $settings->positions->x;
        $src_y = $settings->positions->y;

        if ($src_x <= -$tmp_img_w || $src_x > $src_img_w) {
            $src_x = $src_w = $dst_x = $dst_w = 0;
        } elseif ($src_x <= 0) {
            $dst_x = -$src_x;
            $src_x = 0;
            $src_w = $dst_w = min($src_img_w, $tmp_img_w + $src_x);
        } elseif ($src_x <= $src_img_w) {
            $dst_x = 0;
            $src_w = $dst_w = min($tmp_img_w, $src_img_w - $src_x);
        }

        if ($src_w <= 0 || $src_y <= -$tmp_img_h || $src_y > $src_img_h) {
            $src_y = $src_h = $dst_y = $dst_h = 0;
        } elseif ($src_y <= 0) {
            $dst_y = -$src_y;
            $src_y = 0;
            $src_h = $dst_h = min($src_img_h, $tmp_img_h + $src_y);
        } elseif ($src_y <= $src_img_h) {
            $dst_y = 0;
            $src_h = $dst_h = min($tmp_img_h, $src_img_h - $src_y);
        }

        // Scale to destination position and size
        $ratio = $tmp_img_w / $dst_img_w;
        $dst_x /= $ratio;
        $dst_y /= $ratio;
        $dst_w /= $ratio;
        $dst_w = round($dst_w);
        $dst_h /= $ratio;
        $dst_h = round($dst_h);

        if ($settings->positions->scaleX === -1) {
            $image->flipHorizontally();
        }

        if ($settings->positions->scaleY === -1) {
            $image->flipVertically();
        }

        $filter = ImageInterface::FILTER_LANCZOS;

        if ($image instanceof \Imagine\Gd\Image) {
            $filter = ImageInterface::FILTER_UNDEFINED;
        }

        $image->crop(new Point($src_x, $src_y), new Box($src_w, $src_h));
        $image->resize(new Box($dst_w, $dst_h), $filter);

        $old = $image;

        $image = $imagine->create(new Box($dst_img_w, $dst_img_h), $settings->color);
        $image->paste($old, new Point($dst_x, $dst_y));
    }

    /**
     * Finds the File model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return File the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $modelClass = null)
    {
        if ($modelClass === null) {
            $modelClass = Filegroup::class;
        }

        if (($model = $modelClass::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
