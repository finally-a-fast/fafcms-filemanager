<?php

namespace fafcms\filemanager\models;

use fafcms\filemanager\abstracts\models\BaseArchive;

/**
 * This is the model class for table "{{%archive}}".
 *
 * @package fafcms\filemanager\models
 */
class Archive extends BaseArchive
{

}
