<?php

namespace fafcms\filemanager\abstracts\models;

use fafcms\fafcms\{
    inputs\DateTimePicker,
    inputs\EmailInput,
    inputs\NumberInput,
    inputs\SwitchCheckbox,
    items\ActionColumn,
    items\Card,
    items\Column,
    items\DataColumn,
    items\FormField,
    items\Row,
    items\Tab,
};
use fafcms\filemanager\inputs\FileSelect;
use fafcms\helpers\{
    ActiveRecord,
    interfaces\EditViewInterface,
    interfaces\FieldConfigInterface,
    interfaces\IndexViewInterface,
    traits\AttributeOptionTrait,
    traits\BeautifulModelTrait,
    traits\OptionTrait,
};
use Yii;
use yii\validators\DateValidator;

/**
 * This is the abstract model class for table "{{%archive}}".
 *
 * @package fafcms\filemanager\abstracts\models
 *
 * @property-read array $fieldConfig
 *
 * @property int $id
 * @property int|null $user_id
 * @property string|null $email
 * @property string $files
 * @property int $cron
 * @property int $size
 * @property int $notifications_email
 * @property int $notificaitons_browser
 * @property int $generated
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property string|null $created_at
 * @property string|null $updated_at
 */
abstract class BaseArchive extends ActiveRecord implements FieldConfigInterface, IndexViewInterface, EditViewInterface
{
    use BeautifulModelTrait;
    use OptionTrait;
    use AttributeOptionTrait;

    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataUrl($model): string
    {
        return 'archive';
    }

    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return  'archive';
    }

    /**
     * @inheritDoc
     */
    public static function editDataPlural($model): string
    {
        return Yii::t('fafcms-file', 'Archives');
    }

    /**
     * @inheritDoc
     */
    public static function editDataSingular($model): string
    {
        return Yii::t('fafcms-file', 'Archive');
    }

    /**
     * @inheritDoc
     */
    public static function extendedLabel($model, bool $html = true, array $params = []): string
    {
        return trim(($model['id'] ?? ''));
    }
    //endregion BeautifulModelTrait implementation

    //region OptionTrait implementation
    /**
     * @inheritDoc
     */
    public static function getOptions(bool $empty = true, array $select = null, array $sort = null, array $where = null, array $joinWith = null, string $emptyLabel = null): array
    {
        $options = Yii::$app->dataCache->map(
            static::class,
            array_merge([
                static::tableName() . '.id',
                static::tableName() . '.id'
            ], $select ?? []),
            'id',
            static function ($item) {
                return static::extendedLabel($item);
            },
            null,
            $sort ?? [static::tableName() . '.id' => SORT_ASC],
            $where,
            $joinWith
        );

        return OptionTrait::getOptions($empty, $select, $sort, $where, $joinWith, $emptyLabel) + $options;
    }
    //endregion OptionTrait implementation

    //region AttributeOptionTrait implementation
    /**
     * @inheritDoc
     */
    public function attributeOptions(): array
    {
        return [];
    }
    //endregion AttributeOptionTrait implementation

    //region FieldConfigInterface implementation
    public function getFieldConfig(): array
    {
        return [
            'id' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'user_id' => [
                'type' => NumberInput::class,
            ],
            'email' => [
                'type' => EmailInput::class,
            ],
            'files' => [
                'type' => FileSelect::class,
            ],
            'cron' => [
                'type' => SwitchCheckbox::class,
                'offLabel' => [
                    'fafcms-core',
                    'No',
                ],
                'onLabel' => [
                    'fafcms-core',
                    'Yes',
                ],
            ],
            'size' => [
                'type' => NumberInput::class,
            ],
            'notifications_email' => [
                'type' => SwitchCheckbox::class,
                'offLabel' => [
                    'fafcms-core',
                    'No',
                ],
                'onLabel' => [
                    'fafcms-core',
                    'Yes',
                ],
            ],
            'notificaitons_browser' => [
                'type' => SwitchCheckbox::class,
                'offLabel' => [
                    'fafcms-core',
                    'No',
                ],
                'onLabel' => [
                    'fafcms-core',
                    'Yes',
                ],
            ],
            'generated' => [
                'type' => SwitchCheckbox::class,
                'offLabel' => [
                    'fafcms-core',
                    'No',
                ],
                'onLabel' => [
                    'fafcms-core',
                    'Yes',
                ],
            ],
            'created_by' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'updated_by' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'created_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'updated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
        ];
    }
    //endregion FieldConfigInterface implementation

    //region IndexViewInterface implementation
    public static function indexView(): array
    {
        return [
            'default' => [
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'id',
                        'sort' => 1,
                        'link' => true,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'user_id',
                        'sort' => 2,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'email',
                        'sort' => 3,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'files',
                        'sort' => 4,
                        'link' => true,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'cron',
                        'sort' => 5,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'size',
                        'sort' => 6,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'notifications_email',
                        'sort' => 7,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'notificaitons_browser',
                        'sort' => 8,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'generated',
                        'sort' => 9,
                    ],
                ],
                [
                    'class' => ActionColumn::class,
                ],
            ]
        ];
    }
    //endregion IndexViewInterface implementation

    //region EditViewInterface implementation
    public static function editView(): array
    {
        return [
            'default' => [
                'tab-1' => [
                    'class' => Tab::class,
                    'settings' => [
                        'label' => [
                            'fafcms-core',
                            'Master data',
                        ],
                    ],
                    'contents' => [
                        'row-1' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 8,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-core',
                                                    'Master data',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-user_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'user_id',
                                                    ],
                                                ],
                                                'field-email' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'email',
                                                    ],
                                                ],
                                                'field-files' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'files',
                                                    ],
                                                ],
                                                'field-cron' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'cron',
                                                    ],
                                                ],
                                                'field-size' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'size',
                                                    ],
                                                ],
                                                'field-notifications_email' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'notifications_email',
                                                    ],
                                                ],
                                                'field-notificaitons_browser' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'notificaitons_browser',
                                                    ],
                                                ],
                                                'field-generated' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'generated',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
    //endregion EditViewInterface implementation

    /**
     * {@inheritdoc}
     */
    public static function prefixableTableName(): string
    {
        return '{{%archive}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            'integer-user_id' => ['user_id', 'integer'],
            'integer-size' => ['size', 'integer'],
            'integer-created_by' => ['created_by', 'integer'],
            'integer-updated_by' => ['updated_by', 'integer'],
            'required-files' => ['files', 'required'],
            'required-size' => ['size', 'required'],
            'string-files' => ['files', 'string'],
            'boolean-cron' => ['cron', 'boolean'],
            'boolean-notifications_email' => ['notifications_email', 'boolean'],
            'boolean-notificaitons_browser' => ['notificaitons_browser', 'boolean'],
            'boolean-generated' => ['generated', 'boolean'],
            'date-created_at' => ['created_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'created_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-updated_at' => ['updated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'updated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'string-email' => ['email', 'string', 'max' => 255],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return array_merge(parent::attributeLabels(), [
            'id' => Yii::t('fafcms-file', 'ID'),
            'user_id' => Yii::t('fafcms-file', 'User ID'),
            'email' => Yii::t('fafcms-file', 'Email'),
            'files' => Yii::t('fafcms-file', 'Files'),
            'cron' => Yii::t('fafcms-file', 'Cron'),
            'size' => Yii::t('fafcms-file', 'Size'),
            'notifications_email' => Yii::t('fafcms-file', 'Notifications Email'),
            'notificaitons_browser' => Yii::t('fafcms-file', 'Notificaitons Browser'),
            'generated' => Yii::t('fafcms-file', 'Generated'),
            'created_by' => Yii::t('fafcms-file', 'Created By'),
            'updated_by' => Yii::t('fafcms-file', 'Updated By'),
            'created_at' => Yii::t('fafcms-file', 'Created At'),
            'updated_at' => Yii::t('fafcms-file', 'Updated At'),
        ]);
    }
}
