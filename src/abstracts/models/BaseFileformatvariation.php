<?php

namespace fafcms\filemanager\abstracts\models;

use fafcms\fafcms\{
    inputs\DateTimePicker,
    inputs\NumberInput,
    inputs\TextInput,
    items\ActionColumn,
    items\Card,
    items\Column,
    items\DataColumn,
    items\FormField,
    items\Row,
    items\Tab,
};
use fafcms\filemanager\{
    inputs\FileSelect,
    models\Filesizevariation,
};
use fafcms\helpers\{
    ActiveRecord,
    interfaces\EditViewInterface,
    interfaces\FieldConfigInterface,
    interfaces\IndexViewInterface,
    traits\AttributeOptionTrait,
    traits\BeautifulModelTrait,
    traits\OptionTrait,
};
use Yii;
use yii\db\ActiveQuery;
use yii\validators\DateValidator;

/**
 * This is the abstract model class for table "{{%fileformatvariation}}".
 *
 * @package fafcms\filemanager\abstracts\models
 *
 * @property-read array $fieldConfig
 *
 * @property int $id
 * @property int $filesizevariation_id
 * @property string $variation
 * @property string|null $last_request_at
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property Filesizevariation $filesizevariation
 */
abstract class BaseFileformatvariation extends ActiveRecord implements FieldConfigInterface, IndexViewInterface, EditViewInterface
{
    use BeautifulModelTrait;
    use OptionTrait;
    use AttributeOptionTrait;

    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataUrl($model): string
    {
        return 'fileformatvariation';
    }

    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return  'fileformatvariation';
    }

    /**
     * @inheritDoc
     */
    public static function editDataPlural($model): string
    {
        return Yii::t('fafcms-file', 'Fileformatvariations');
    }

    /**
     * @inheritDoc
     */
    public static function editDataSingular($model): string
    {
        return Yii::t('fafcms-file', 'Fileformatvariation');
    }

    /**
     * @inheritDoc
     */
    public static function extendedLabel($model, bool $html = true, array $params = []): string
    {
        return trim(($model['id'] ?? ''));
    }
    //endregion BeautifulModelTrait implementation

    //region OptionTrait implementation
    /**
     * @inheritDoc
     */
    public static function getOptions(bool $empty = true, array $select = null, array $sort = null, array $where = null, array $joinWith = null, string $emptyLabel = null): array
    {
        $options = Yii::$app->dataCache->map(
            static::class,
            array_merge([
                static::tableName() . '.id',
                static::tableName() . '.id'
            ], $select ?? []),
            'id',
            static function ($item) {
                return static::extendedLabel($item);
            },
            null,
            $sort ?? [static::tableName() . '.id' => SORT_ASC],
            $where,
            $joinWith
        );

        return OptionTrait::getOptions($empty, $select, $sort, $where, $joinWith, $emptyLabel) + $options;
    }
    //endregion OptionTrait implementation

    //region AttributeOptionTrait implementation
    /**
     * @inheritDoc
     */
    public function attributeOptions(): array
    {
        return [
            'filesizevariation_id' => static function(...$params) {
                return Filesizevariation::getOptions(...$params);
            },
        ];
    }
    //endregion AttributeOptionTrait implementation

    //region FieldConfigInterface implementation
    public function getFieldConfig(): array
    {
        return [
            'id' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'filesizevariation_id' => [
                'type' => FileSelect::class,
                'external' => false,
            ],
            'variation' => [
                'type' => TextInput::class,
            ],
            'last_request_at' => [
                'type' => DateTimePicker::class,
            ],
            'created_by' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'updated_by' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'created_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'updated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
        ];
    }
    //endregion FieldConfigInterface implementation

    //region IndexViewInterface implementation
    public static function indexView(): array
    {
        return [
            'default' => [
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'id',
                        'sort' => 1,
                        'link' => true,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'filesizevariation_id',
                        'sort' => 2,
                        'link' => true,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'variation',
                        'sort' => 3,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'last_request_at',
                        'sort' => 4,
                    ],
                ],
                [
                    'class' => ActionColumn::class,
                ],
            ]
        ];
    }
    //endregion IndexViewInterface implementation

    //region EditViewInterface implementation
    public static function editView(): array
    {
        return [
            'default' => [
                'tab-1' => [
                    'class' => Tab::class,
                    'settings' => [
                        'label' => [
                            'fafcms-core',
                            'Master data',
                        ],
                    ],
                    'contents' => [
                        'row-1' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 8,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-core',
                                                    'Master data',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-filesizevariation_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'filesizevariation_id',
                                                    ],
                                                ],
                                                'field-variation' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'variation',
                                                    ],
                                                ],
                                                'field-last_request_at' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'last_request_at',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
    //endregion EditViewInterface implementation

    /**
     * {@inheritdoc}
     */
    public static function prefixableTableName(): string
    {
        return '{{%fileformatvariation}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            'required-filesizevariation_id' => ['filesizevariation_id', 'required'],
            'required-variation' => ['variation', 'required'],
            'integer-filesizevariation_id' => ['filesizevariation_id', 'integer'],
            'integer-created_by' => ['created_by', 'integer'],
            'integer-updated_by' => ['updated_by', 'integer'],
            'date-last_request_at' => ['last_request_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'last_request_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-created_at' => ['created_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'created_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-updated_at' => ['updated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'updated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'string-variation' => ['variation', 'string', 'max' => 255],
            'exist-filesizevariation_id' => [['filesizevariation_id'], 'exist', 'skipOnError' => true, 'targetClass' => Filesizevariation::class, 'targetAttribute' => ['filesizevariation_id' => 'id']],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return array_merge(parent::attributeLabels(), [
            'id' => Yii::t('fafcms-file', 'ID'),
            'filesizevariation_id' => Yii::t('fafcms-file', 'Filesizevariation ID'),
            'variation' => Yii::t('fafcms-file', 'Variation'),
            'last_request_at' => Yii::t('fafcms-file', 'Last Request At'),
            'created_by' => Yii::t('fafcms-file', 'Created By'),
            'updated_by' => Yii::t('fafcms-file', 'Updated By'),
            'created_at' => Yii::t('fafcms-file', 'Created At'),
            'updated_at' => Yii::t('fafcms-file', 'Updated At'),
        ]);
    }

    /**
     * Gets query for [[Filesizevariation]].
     *
     * @return ActiveQuery
     */
    public function getFilesizevariation(): ActiveQuery
    {
        return $this->hasOne(Filesizevariation::class, [
            'id' => 'filesizevariation_id',
        ]);
    }
}
