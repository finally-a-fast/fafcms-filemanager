<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 - 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-filemanager/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-filemanager
 * @see https://www.finally-a-fast.com/packages/fafcms-filemanager/docs Documentation of fafcms-filemanager
 * @since File available since Release 1.0.0
 */

declare(strict_types=1);

namespace fafcms\filemanager\assets;

use fafcms\helpers\classes\AssetComponentBundle;

/**
 * Class FilemanagerAsset
 *
 * @package fafcms\filemanager\assets
 */
class FilemanagerAsset extends AssetComponentBundle
{
    public $sourcePath = __DIR__ . '/filemanager';

    public $css = [
        'filemanager.scss',
    ];

    public $depends = [
        FilemanagerSelectAsset::class,
        FilemanagerUploadAsset::class
    ];
}
